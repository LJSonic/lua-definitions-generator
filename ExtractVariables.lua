-- Todo:
-- Value (Hex)

local parsePage = require 'parsePage'
local util = require 'util'

local err = util.printerr

local manualVariableNames = {
	sfxinfo = 'sfxinfo',
}

local manualVariableValues = {
}

local manualVariableTypes = {
	sprnames  = 'table<integer|string, integer|string>',
	spr2names = 'table<integer|string, integer|string>',
	skins     = 'table<integer|string, skin_t>',
}

local ignoredVariables = util.arrayToSet{
}

local doc = {}

local function appendDoc(s)
	table.insert(doc, s)
end

local function extractVariable(row, isArray)
	local name = util.wikiToPlainString(row.cells[1].content)

	if ignoredVariables[name:match('[%w_]+')] then
		return
	end

	name = manualVariableNames[name:match('[%w_]+')] or name

	if not name:match('^[%w_]+%s*$') then
		err('Failed to parse variable name:')
		err(name)
		err()
		return
	end

	local varType = util.wikiToPlainString(row.cells[2].content)
	if manualVariableTypes[name] then
		varType = manualVariableTypes[name]
	elseif varType:match('^[%w_]+%s*$') then
		if isArray then
			varType = varType .. '[]'
		end
	else
		err('Failed to parse type for variable ' .. name .. ':')
		err(varType)
		err()
		return
	end

	local wikiLink = 'Lua/Global_variables#' ..
		(isArray and 'Tables' or 'Variables')

	local description = row.cells[isArray and 5 or 4].content
	description = util.extractDescription(description, 'Global_variables', wikiLink)

	appendDoc(
		description ..
		'---@type ' .. varType .. '\n' ..
		name .. ' = ' .. (isArray and '{}' or 'nil') .. '\n'
	)
end

local function extractVariables(t)
	for i = 2, #t.rows do
		local isArray = util.wikiToPlainString(t.rows[1].cells[2].content) == 'Array type'
		extractVariable(t.rows[i], isArray)
	end

	appendDoc('\n')
end

local page = parsePage(util.readFile('wiki/Global_variables.txt'))
for _, t in ipairs(page) do
	extractVariables(t)
end

doc = table.concat(doc):gsub('\n*$', '\n')
util.writeFile('defs/Variables.lua', doc)
