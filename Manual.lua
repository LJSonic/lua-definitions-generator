---@meta


---@class SINT8  : integer
---@class UINT8  : integer
---@class INT16  : integer
---@class UINT16 : integer
---@class INT32  : integer
---@class UINT32 : integer

---@class fixed_t : INT32
---@class angle_t : UINT32
---@class tic_t   : INT32

---@class ffloorflags_t  : integer
---@class mobjtype_t     : integer
---@class playeranim_t   : integer
---@class playerflags_t  : integer
---@class playerstate_t  : integer
---@class rotaxis_t      : integer
---@class statenum_t     : integer
---@class skinflags_t    : integer
---@class soundnum_t     : integer
---@class skincolornum_t : integer
---@class spritenum_t    : integer


---@class easelib
ease = {}

---@class hudlib
hud = {}

---@class inputlib
input = {}

---@class videolib
local v = {}


---@alias hooktype
---| '"IntermissionThinker"'
---| '"LinedefExecute"'
---| '"MapChange"'
---| '"MapLoad"'
---| '"ThinkFrame"'
---| '"PreThinkFrame"'
---| '"PostThinkFrame"'
---| '"GameQuit"'
---| '"BotAI"'
---| '"BotTiccmd"'
---| '"BotRespawn"'
---| '"HurtMsg"'
---| '"NetVars"'
---| '"PlayerJoin"'
---| '"PlayerQuit"'
---| '"PlayerMsg"'
---| '"TeamSwitch"'
---| '"ViewpointSwitch"'
---| '"SeenPlayer"'
---| '"PlayerThink"'
---| '"PlayerCanDamage"'
---| '"PlayerSpawn"'
---| '"PlayerHeight"'
---| '"PlayerCanEnterSpinGaps"'
---| '"ShieldSpawn"'
---| '"AbilitySpecial"'
---| '"JumpSpecial"'
---| '"JumpSpinSpecial"'
---| '"SpinSpecial"'
---| '"ShieldSpecial"'
---| '"MapThingSpawn"'
---| '"MobjSpawn"'
---| '"MobjRemoved"'
---| '"MobjThinker"'
---| '"MobjFuse"'
---| '"BossThinker"'
---| '"ShouldDamage"'
---| '"MobjDamage"'
---| '"MobjDeath"'
---| '"BossDeath"'
---| '"FollowMobj"'
---| '"MobjCollide"'
---| '"MobjLineCollide"'
---| '"MobjMoveBlocked"'
---| '"MobjMoveCollide"'
---| '"TouchSpecial"'
---| '"MusicChange"'
---| '"ShouldJingleContinue"'
---| '"KeyDown"'
---| '"KeyUp"'
---| '"PlayerCmd"'
---| '"HUD"'

---@alias hudtype
---| '"game"'' function(*[drawer](https://wiki.srb2.org/wiki/Lua/Functions#Patch/string_drawing_functions)* v, [*[player_t](https://wiki.srb2.org/wiki/Lua/Userdata_structures#player_t)* stplyr, [*[camera_t](https://wiki.srb2.org/wiki/Lua/Userdata_structures#camera_t)* cam]])
---| '"scores"'' function(*[drawer](https://wiki.srb2.org/wiki/Lua/Functions#Patch/string_drawing_functions)* v)
---| '"title"'' function(*[drawer](https://wiki.srb2.org/wiki/Lua/Functions#Patch/string_drawing_functions)* v)
---| '"titlecard"'' function(*[drawer](https://wiki.srb2.org/wiki/Lua/Functions#Patch/string_drawing_functions)* v, [*[player_t](https://wiki.srb2.org/wiki/Lua/Userdata_structures#player_t)* stplyr, [*int* ticker, [*int* endtime]]])

---@alias huditemtype
---| '"stagetitle"' Stage title card
---| '"textspectator"' Information text as a spectator
---| '"crosshair"' Crosshair in first-person
---| '"score"' Score text and counter
---| '"time"' Time text and counter
---| '"rings"' Rings text and counter
---| '"lives"' Lives picture and counter
---| '"teamscores"' Team Match/CTF - Team scores
---| '"weaponrings"' Match/CTF - Weapon ring icons
---| '"powerstones"' Match/CTF - Chaos Emerald icons
---| '"nightslink"' NiGHTS link counter
---| '"nightsdrill"' NiGHTS drill bar
---| '"nightsrings"' NiGHTS ring counter
---| '"nightsscore"' NiGHTS score counter
---| '"nightstime"' NiGHTS time counter
---| '"nightsrecords"' NiGHTS screen text (Bonus time start, "Get *n* [more] Spheres", Ending bonuses)
---| '"rankings"' https://wiki.srb2.org/wiki/Rankings/Scores_HUD - Multiplayer rankings (all gametypes)
---| '"coopemeralds"' Rankings/Scores HUD - Single Player Chaos Emerald icons
---| '"tokens"' Rankings/Scores HUD - Single Player Tokens icon and counter
---| '"tabemblems"' Rankings/Scores HUD - Single Player Emblems icon and counter
---| '"intermissiontally"' Intermission - Score tally
---| '"intermissionmessages"' Intermission - Information text
---| '"intermissiontitletext"' Intermission - Co-Op/Special Stage title text
---| '"intermissionemeralds"' Intermission - Special Stage emeralds

---@alias stringtype
---| '"left"' (*Default value*) The string is left-aligned; the X coordinate is the left edge of the string graphic.
---| '"right"' The string is right-aligned; the X coordinate is the right edge of the string graphic.
---| '"center"' The string is center-aligned; the X coordinate is the center of the string graphic.
---| '"fixed"' The string is left-aligned; the X coordinate is the left edge of the string graphic and coordinates are required to be treated as fixed-point values instead of normal integers, allowing for positions at fractions of a pixel on the screen. (e.g., `FRACUNIT` is one pixel, `FRACUNIT/2` is half a pixel, `2*FRACUNIT` is two pixels, etc.).
---| '"fixed-center"' The string is center-aligned; the X coordinate is the center of the string graphic and coordinates are required to be treated as fixed-point values instead of normal integers, allowing for positions at fractions of a pixel on the screen. (e.g., `FRACUNIT` is one pixel, `FRACUNIT/2` is half a pixel, `2*FRACUNIT` is two pixels, etc.).
---| '"fixed-right"' The string is right-aligned; the X coordinate is the right edge of the string graphic and coordinates are required to be treated as fixed-point values instead of normal integers, allowing for positions at fractions of a pixel on the screen. (e.g., `FRACUNIT` is one pixel, `FRACUNIT/2` is half a pixel, `2*FRACUNIT` is two pixels, etc.).
---| '"small"' The string is drawn in a small font, and is left-aligned.
---| '"small-center"' The string is drawn in a small font, and is center-aligned.
---| '"small-right"' The string is drawn in a small font, and is right-aligned.
---| '"small-fixed"' The string is drawn in a small font, and is left-aligned and coordinates are required to be treated as fixed-point values instead of normal integers, allowing for positions at fractions of a pixel on the screen. (e.g., `FRACUNIT` is one pixel, `FRACUNIT/2` is half a pixel, `2*FRACUNIT` is two pixels, etc.).
---| '"small-fixed-center"' The string is drawn in a small font, and is center-aligned and coordinates are required to be treated as fixed-point values instead of normal integers, allowing for positions at fractions of a pixel on the screen. (e.g., `FRACUNIT` is one pixel, `FRACUNIT/2` is half a pixel, `2*FRACUNIT` is two pixels, etc.).
---| '"small-fixed-right"' The string is drawn in a small font, and is right-aligned and coordinates are required to be treated as fixed-point values instead of normal integers, allowing for positions at fractions of a pixel on the screen. (e.g., `FRACUNIT` is one pixel, `FRACUNIT/2` is half a pixel, `2*FRACUNIT` is two pixels, etc.).
---| '"small-thin"' The string is drawn in a small and thin font, and is left-aligned.
---| '"small-thin-center"' The string is drawn in a small and thin font, and is center-aligned.
---| '"small-thin-right"' The string is drawn in a small and thin font, and is right-aligned.
---| '"small-thin-fixed"' The string is drawn in a small and thin font, and is left-aligned and coordinates are required to be treated as fixed-point values instead of normal integers, allowing for positions at fractions of a pixel on the screen. (e.g., `FRACUNIT` is one pixel, `FRACUNIT/2` is half a pixel, `2*FRACUNIT` is two pixels, etc.).
---| '"small-thin-fixed-center"' The string is drawn in a small and thin font, and is center-aligned and coordinates are required to be treated as fixed-point values instead of normal integers, allowing for positions at fractions of a pixel on the screen. (e.g., `FRACUNIT` is one pixel, `FRACUNIT/2` is half a pixel, `2*FRACUNIT` is two pixels, etc.).
---| '"small-thin-fixed-right"' The string is drawn in a small and thin font, and is right-aligned and coordinates are required to be treated as fixed-point values instead of normal integers, allowing for positions at fractions of a pixel on the screen. (e.g., `FRACUNIT` is one pixel, `FRACUNIT/2` is half a pixel, `2*FRACUNIT` is two pixels, etc.).
---| '"thin"' The string is drawn in a thin font, and is left-aligned.
---| '"thin-center"' The string is drawn in a thin font, and is center-aligned.
---| '"thin-right"' The string is drawn in a thin font, and is right-aligned.
---| '"thin-fixed"' The string is drawn in a thin font, and is left-aligned and coordinates are required to be treated as fixed-point values instead of normal integers, allowing for positions at fractions of a pixel on the screen. (e.g., `FRACUNIT` is one pixel, `FRACUNIT/2` is half a pixel, `2*FRACUNIT` is two pixels, etc.).
---| '"thin-fixed-center"' The string is drawn in a thin font, and is center-aligned and coordinates are required to be treated as fixed-point values instead of normal integers, allowing for positions at fractions of a pixel on the screen. (e.g., `FRACUNIT` is one pixel, `FRACUNIT/2` is half a pixel, `2*FRACUNIT` is two pixels, etc.).
---| '"thin-fixed-right"' The string is drawn in a thin font, and is right-aligned and coordinates are required to be treated as fixed-point values instead of normal integers, allowing for positions at fractions of a pixel on the screen. (e.g., `FRACUNIT` is one pixel, `FRACUNIT/2` is half a pixel, `2*FRACUNIT` is two pixels, etc.).

---@alias stringwidthtype
---| '"normal"' (*Default value*) The string is drawn in a normal font.
---| '"small"' The string is drawn in a small font.
---| '"thin"' The string is drawn in a thin font.


---@class vector2_t
---@field x fixed_t
---@field y fixed_t

---@class vector3_t
---@field x fixed_t
---@field y fixed_t
---@field z fixed_t

-- These are not documented anywhere?
-- Also... no _t suffix for colormap??
---@class spriteinfo_t
---@class playersprite_t
---@class colormap


ANG1 = 11930465
ANG2 = 23860929
ANG10 = 119304647
ANG15 = 178956971
ANG20 = 238609294
ANG30 = 357913941
ANG60 = 715827883
ANG64h = 769514974
ANG105 = 1252698795
ANG210 = -1789569707
ANG255 = -1252698795
ANG340 = -238609294
ANG350 = -119304647

ANGLE_11hh = 134217728
ANGLE_22h = 268435456
ANGLE_45 = 536870912
ANGLE_67h = 805306368
ANGLE_90 = 1073741824
ANGLE_112h = 1342177280
ANGLE_135 = 1610612736
ANGLE_157h = 1879048192
ANGLE_180 = -2147483648
ANGLE_202h = -1879048192
ANGLE_225 = -1610612736
ANGLE_247h = -1342177280
ANGLE_270 = -1073741824
ANGLE_292h = -805306368
ANGLE_315 = -536870912
ANGLE_337h = -268435456
ANGLE_MAX = -1

A = 0
B = 1
C = 2
D = 3
E = 4
F = 5
G = 6
H = 7
I = 8
J = 9
K = 10
L = 11
M = 12
N = 13
O = 14
P = 15
Q = 16
R = 17
S = 18
T = 19
U = 20
V = 21
W = 22
X = 23
Y = 24
Z = 25


---@type mobj_t[]
mobjs = {}


---
---**[View on wiki](https://wiki.srb2.org/wiki/A_BubbleRise)**
---
--- Used to make bubbles rise.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_BubbleRise(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_BunnyHop)**
---
--- Makes the actor hop like a bunny.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_BunnyHop(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CapeChase)**
---
--- Sets the actor's position to that of its target or tracer. If used repeatedly, the actor will continuously follow its target/tracer.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CapeChase(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Chase)**
---
--- Makes the actor move towards its target. Use this repeatedly for basic chasing movement. Used by several enemies, such as the Crawlas.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Chase(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FishJump)**
---
--- Makes the actor jump if on the ground or underwater. Used by the SDURF.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FishJump(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_ForceStop)**
---
--- Immediately stops the actor's movement.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_ForceStop(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_HomingChase)**
---
--- Makes the actor move directly towards its target or tracer. Use this repeatedly to make the actor continuously chase its target.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_HomingChase(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_InstaLoop)**
---
--- Makes the actor move along a 2D horizontal polygon.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_InstaLoop(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_KnockBack)**
---
--- Knocks back the actor's target or tracer at its current speed.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_KnockBack(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_MouseThink)**
---
--- The thinker for mice released by enemies.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_MouseThink(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_MoveAbsolute)**
---
--- Moves the actor horizontally at an absolute speed.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_MoveAbsolute(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_MoveRelative)**
---
--- Moves the actor horizontally while keeping its current momentum.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_MoveRelative(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_PushAway)**
---
--- Pushes the actor's target or tracer away from the actor itself.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_PushAway(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_ScoreRise)**
---
--- Used to make score logos rise.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_ScoreRise(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SkullAttack)**
---
--- Makes the actor fly like a missile toward its target.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SkullAttack(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Thrust)**
---
--- Thrusts the actor horizontally at its current angle.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Thrust(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_ZThrust)**
---
--- Thrusts the actor vertically.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_ZThrust(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Custom3DRotate)**
---
--- Rotates the actor around its target in three dimensions.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Custom3DRotate(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_OrbitNights)**
---
--- Makes the Chaos Emeralds in NiGHTS Special Stages orbit around Super Sonic.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_OrbitNights(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_RotateSpikeBall)**
---
--- Rotates the actor around its target or tracer.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_RotateSpikeBall(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SparkFollow)**
---
--- Rotates the actor around its target; requires the target to be a player in Super form.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SparkFollow(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/---@param var2 INT32)**
---
--- Thinker for the Unidus's/Egg Mobile's spikeballs. Rotates the actor around its target. Depending on the situation, the actor can be thrown away from its target.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_UnidusBall(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_BuzzFly)**
---
--- Thinker for the golden and red Buzzes. Makes the actor slowly fly after the player.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_BuzzFly(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CrawlaCommanderThink)**
---
--- Thinker for the Crawla Commander.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CrawlaCommanderThink(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_DetonChase)**
---
--- Thinker for the Deton. Makes the actor chase after a player and explode if it comes into contact with the player.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_DetonChase(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_EggShield)**
---
--- Thinker for Egg Guard's shield. Sets the position of the actor to its target and pushes away any players touching it at the front.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_EggShield(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FaceStabChase)**
---
--- Modified A_Chase for the CastleBot FaceStabber.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FaceStabChase(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_GuardChase)**
---
--- Modified A_Chase for the Egg Guard.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_GuardChase(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_HoodThink)**
---
--- Thinker for the Robo-Hood.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_HoodThink(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_JetbThink)**
---
--- Thinker for the Jetty-Syn Bomber.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_JetbThink(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_JetChase)**
---
--- Modified A_Chase for the Jetty-Syns.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_JetChase(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_JetgShoot)**
---
--- Firing attack for the Jetty-Syn Gunner.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_JetgShoot(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_JetgThink)**
---
--- Thinker for the Jetty-Syn Gunner.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_JetgThink(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_JetJawChomp)**
---
--- Attacking thinker for the Jet Jaw. Chases towards the actor's target as long as it is in view.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_JetJawChomp(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_JetJawRoam)**
---
--- Roaming thinker for the Jet Jaw.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_JetJawRoam(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_MinusCheck)**
---
--- Used to reset the Minus to digging form when it hits the floor.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_MinusCheck(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_MinusDigging)**
---
--- Used by the Minus when digging in the ground.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_MinusDigging(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_MinusPopup)**
---
--- Used by the Minus when popping out of the ground.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_MinusPopup(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_PointyThink)**
---
--- Thinker for the Pointy.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_PointyThink(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SharpChase)**
---
--- Thinker for the Sharp.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SharpChase(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SharpSpin)**
---
--- Thinker for the Sharp's spin attack.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SharpSpin(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SkimChase)**
---
--- Modified A_Chase for the Skim.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SkimChase(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SnailerThink)**
---
--- Thinker for the Snailer.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SnailerThink(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_VultureCheck)**
---
--- Thinker for the BASH's flight. If stopped, the actor falls down until it reaches the ground.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_VultureCheck(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_VultureVtol)**
---
--- Thinker for the BASH when aiming. The actor adjusts its Z position until a suitable height to attack its target from is reached.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_VultureVtol(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss1Chase)**
---
--- Basic chasing behavior and thinker used by the Egg Mobile. An attack may also be randomly selected by this action.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss1Chase(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/---@param var2 INT32)**
---
--- Egg Mobile's laser attack thinker. Unlike other actions, this action will automatically call itself again repeatedly for the rest of the current state's duration.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss1Laser(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss1Spikeballs)**
---
--- Used to spawn each of the Egg Mobile's spikeballs (once per use of the action) during pinch phase.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss1Spikeballs(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss2Chase)**
---
--- Egg Slimer's main thinker, for moving in a circle around an Axis and spraying goop around itself.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss2Chase(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss2Pogo)**
---
--- Egg Slimer's pinch phase thinker, for pogoing around and spraying goop around itself.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss2Pogo(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss2PogoSFX)**
---
--- Old pogoing thrust behavior used by the Egg Slimer in previous versions of SRB2.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss2PogoSFX(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss2PogoTarget)**
---
--- New pogoing thrust behavior used by the Egg Slimer from v2.1 onwards.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss2PogoTarget(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss2TakeDamage)**
---
--- Egg Slimer's pain-invincibility handler; prevents the actor from being damaged again for a limited time.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss2TakeDamage(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss3Path)**
---
--- Used by the fake Sea Egg clones to behave similarly to the Sea Egg itself, but taking into account where the Sea Egg itself is actually located.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss3Path(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss3TakeDamage)**
---
--- Sea Egg's pain-invincibility handler; allows it to start moving along the paths of boss waypoints again.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss3TakeDamage(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss4Raise)**
---
--- Used by the Eggscalibur to signal when to raise the cage surrounding it.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss4Raise(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss4Reverse)**
---
--- Used by the Eggscalibur to signal when to reverse the direction its spikeball arms spin around it.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss4Reverse(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss4SpeedUp)**
---
--- Used by the Eggscalibur to signal when to increase the speed its spikeball arms spin around it.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss4SpeedUp(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss7Chase)**
---
--- Modified A_Chase for v2.0's Brak Eggman.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss7Chase(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss7FireMissiles)**
---
--- Fires four missiles of a specified type from around the actor's middle. Used by v2.0's Brak Eggman to fire grabbable missiles at the player.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss7FireMissiles(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_BossDeath)**
---
--- Thinker for bosses after they have been defeated.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_BossDeath(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/---@param var2 INT32)**
---
--- A specialized firing attack that allows the Egg Mobile (in previous versions of SRB2) and the Sea Egg to fire missiles from specific positions relative to themselves.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_BossFireShot(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_BossJetFume)**
---
--- Used to spawn Egg Mobile-style jet fumes, a Sea Egg-style propeller, a Metal Sonic-style jet fume, or an Eggscalibur-style jet flame.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_BossJetFume(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_BossScream)**
---
--- Used to spawn explosions around the actor during a boss death animation.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_BossScream(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_BossZoom)**
---
--- An unused action similar in function to A_SkullAttack, but with a modifiable speed.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_BossZoom(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_BrakChase)**
---
--- Modified A_Chase for Brak Eggman; movement speed and attacks depend on the actor's health.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_BrakChase(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_BrakFireShot)**
---
--- Makes the actor shoot an Object at its target, offset to match where Brak Eggman's gun is.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_BrakFireShot(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/---@param var2 INT32)**
---
--- A specialized version of A_LobShot used by Brak Eggman. The actor lobs an Object at the floor a third of the way towards the target, assuming it will bounce the rest of the way.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_BrakLobShot(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FocusTarget)**
---
--- Used by the Egg Mobile's laser to move towards the target player.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FocusTarget(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_AttractChase)**
---
--- Thinker for rings, CTF team rings (red and blue) and coins. If a nearby player has an Attraction Shield, the actor will be pulled towards it.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_AttractChase(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_OldRingExplode)**
---
--- Makes the actor explode like the old Explosion Ring from v1.09.4.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_OldRingExplode(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_RingDrain)**
---
--- Removes a specified number of rings from the actor's target player.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_RingDrain(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_RingExplode)**
---
--- Makes the actor explode like an Explosion Ring or Grenade Ring.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_RingExplode(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_ThrownRing)**
---
--- Thinker for thrown rings.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_ThrownRing(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_1upThinker)**
---
--- Changes actor's sprite to nearest player's 1-up sprite.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_1upThinker(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_AwardScore)**
---
--- Awards a specified amount of points to a player.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_AwardScore(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CustomPower)**
---
--- Gives the target player a power-up.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CustomPower(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_EggmanBox)**
---
--- Deals non-elemental damage to the target player. Used by the Eggman Monitor.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_EggmanBox(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_ExtraLife)**
---
--- Gives the target player an extra life.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_ExtraLife(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_GiveShield)**
---
--- Gives the target player a shield.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_GiveShield(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_GiveWeapon)**
---
--- Gives the target player one or more weapon panels.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_GiveWeapon(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_GravityBox)**
---
--- Reverses the target player's gravity for the specified time.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_GravityBox(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Invincibility)**
---
--- Makes the target player invincible.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Invincibility(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_MixUp)**
---
--- Used as a thinker for the Teleport Monitor.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_MixUp(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_MonitorPop)**
---
--- Used as a thinker for when a monitor is popped.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_MonitorPop(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_RecyclePowers)**
---
--- Used as thinker for the Recycler Monitor.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_RecyclePowers(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_RingBox)**
---
--- Gives the target player a certain amount of rings.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_RingBox(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SuperSneakers)**
---
--- Gives the target player Super Sneakers.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SuperSneakers(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_BubbleSpawn)**
---
--- Spawns a randomly sized bubble if underwater. Used by the Air Bubble Patch.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_BubbleSpawn(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FanBubbleSpawn)**
---
--- Spawns a randomly sized bubble if underwater. Used by the Fan.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FanBubbleSpawn(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_GhostMe)**
---
--- Spawns an afterimage of the actor's current position.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_GhostMe(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_RockSpawn)**
---
--- Spawns rocks at a specified interval.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_RockSpawn(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SmokeTrailer)**
---
--- Spawns a trail of Objects when used repeatedly.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SmokeTrailer(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SpawnFreshCopy)**
---
--- Spawns a copy of the actor. Used to respawn Brak Eggman's electric barrier.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SpawnFreshCopy(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SpawnObjectAbsolute)**
---
--- Spawns an Object at an absolute location.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SpawnObjectAbsolute(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SpawnObjectRelative)**
---
--- Spawns an Object relative to the actor's position.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SpawnObjectRelative(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_ConnectToGround)**
---
--- Spawns as many Objects as needed between an Object and the nearest floor or ceiling.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_ConnectToGround(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_DropMine)**
---
--- Makes the actor drop an Object.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_DropMine(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FireShot)**
---
--- Fires an Object at the actor's target.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FireShot(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_LobShot)**
---
--- Lobs an Object at the actor's target.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_LobShot(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_MissileSplit)**
---
--- When applied to a missile Object, creates a second missile.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_MissileSplit(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_MultiShot)**
---
--- Shoots multiple Objects horizontally that spread evenly.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_MultiShot(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_NapalmScatter)**
---
--- Scatters a number of projectiles around in a circle; intended for use with Objects that are affected by gravity.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_NapalmScatter(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_ShootBullet)**
---
--- Makes the actor fire an Object at its target.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_ShootBullet(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SplitShot)**
---
--- Shoots a missile at either side of the actor's target.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SplitShot(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SuperFireShot)**
---
--- Fires an Object at the actor's target that will even stun Super Sonic.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SuperFireShot(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SuperTurretFire)**
---
--- Rapidly fires projectiles that will even stun Super Sonic.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SuperTurretFire(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_TrapShot)**
---
--- Fires a projectile in a particular direction, rather than at an Object.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_TrapShot(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_TurretFire)**
---
--- Rapidly fires projectiles at the actor's target.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_TurretFire(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_TurretStop)**
---
--- Stops A_TurretFire or A_SuperTurretFire.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_TurretStop(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_VileAttack)**
---
--- Instantly hurts the actor's target if it is in the actor's line of sight. Used as part of Brak Eggman's sniping attack.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_VileAttack(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/---@param var2 INT32)**
---
--- Thinker for Brak Eggman's targeting reticule. Keeps the actor in front of its tracer, unless its target cannot see it. Optionally draws a line directly to the actor's target.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_VileFire(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_VileTarget)**
---
--- Spawns an Object directly on the actor's target and sets this Object as the actor's tracer. Used as part of Brak Eggman's sniping attack.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_VileTarget(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CanarivoreGas)**
---
--- Spawns an object at the actor's X, Y, and Z coordinates.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CanarivoreGas(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Pain)**
---
--- Plays the actor's PainSound.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Pain(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_PlayActiveSound)**
---
--- Plays the actor's ActiveSound
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_PlayActiveSound(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_PlayAttackSound)**
---
--- Plays the actor's AttackSound.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_PlayAttackSound(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_PlaySeeSound)**
---
--- Plays the actor's SeeSound.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_PlaySeeSound(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_PlaySound)**
---
--- Plays a sound effect.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_PlaySound(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Scream)**
---
--- Plays the actor's DeathSound.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Scream(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_DualAction)**
---
--- Performs two actions simultaneously.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_DualAction(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Shockwave)**
---
--- Spawns a shockwave of objects. Best used to spawn objects that call A_Boss3ShockThink.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Shockwave(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_LinedefExecute)**
---
--- Executes a linedef executor.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_LinedefExecute(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_RandomState)**
---
--- Randomly chooses between two states.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_RandomState(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_RandomStateRange)**
---
--- Randomly chooses a state from a range.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_RandomStateRange(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_RemoteAction)**
---
--- Tells the actor's target to perform an action remotely.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_RemoteAction(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_RemoteDamage)**
---
--- Damages, kills or removes the actor or its target/tracer remotely.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_RemoteDamage(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SetTargetsTarget)**
---
--- Changes the actor's target to the target of its current target.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SetTargetsTarget(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SetObjectState)**
---
--- Sets the state of the actor's target.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SetObjectState(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SetObjectTypeState)**
---
--- Sets the state of Objects of a certain Object type.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SetObjectTypeState(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_ChangeAngleAbsolute)**
---
--- Sets the actor's angle.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_ChangeAngleAbsolute(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_ChangeAngleRelative)**
---
--- Sets the actor's angle relative to its current angle.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_ChangeAngleRelative(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_ChangeColorAbsolute)**
---
--- Sets the actor's skin color.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_ChangeColorAbsolute(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_ChangeColorRelative)**
---
--- Sets the actor's skin color relative to its current skin color.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_ChangeColorRelative(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FindTarget)**
---
--- Sets the actor's target.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FindTarget(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FindTracer)**
---
--- Sets the actor's tracer.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FindTracer(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SetFuse)**
---
--- Set the actor's fuse timer.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SetFuse(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SetObjectFlags)**
---
--- Modifies the actor's primary Object flags.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SetObjectFlags(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SetObjectFlags2)**
---
--- Modifies the actor's secondary Object flags.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SetObjectFlags2(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SetRandomTics)**
---
--- Randomly sets the duration of the actor's current state.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SetRandomTics(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SetReactionTime)**
---
--- Sets the actor's reaction time.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SetReactionTime(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SetScale)**
---
--- Sets the actor's scale.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SetScale(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SetTics)**
---
--- Sets the duration of the actor's current state.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SetTics(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_BubbleCheck)**
---
--- Checks if the actor is underwater; if not, the actor turns invisible.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_BubbleCheck(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CheckAmbush)**
---
--- Calls a specified state if the actor is behind its target.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CheckAmbush(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CheckHealth)**
---
--- Calls a specified state depending on the actor's current health.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CheckHealth(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CheckHeight)**
---
--- Calls a specified state depending on the height difference between the actor and its target.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CheckHeight(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CheckRandom)**
---
--- Randomly decides whether or not to call a specified state.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CheckRandom(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CheckRange)**
---
--- Calls a specified state depending on the actor's horizontal distance to its target.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CheckRange(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CheckRings)**
---
--- Calls a specified state depending on the total number of rings held by all players.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CheckRings(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CheckTargetRings)**
---
--- Calls a specified state depending on the number of rings held by the actor's target.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CheckTargetRings(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CheckThingCount)**
---
--- Calls a specified state depending on number of active Objects of a specified Object type.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CheckThingCount(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CheckTotalRings)**
---
--- Calls a specified state if the total number of rings collected by all players is high enough.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CheckTotalRings(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CheckTrueRange)**
---
--- Calls a specified state if the actor's target is in range.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CheckTrueRange(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_ChickenCheck)**
---
--- Resets a chicken if it is on the ground.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_ChickenCheck(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Look)**
---
--- Looks for a player and sets them as the actor's new target.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Look(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SearchForPlayers)**
---
--- Calls a specified state if the actor has found a player.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SearchForPlayers(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CheckCustomValue)**
---
--- Calls a specified state depending on the actor's custom value.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CheckCustomValue(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CheckCusValMemo)**
---
--- Calls a specified state depending on the actor's custom memory value.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CheckCusValMemo(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CusValAction)**
---
--- Remotely calls an action, but modifies Var1 and Var2 depending on the actor's custom value.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CusValAction(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_RelayCustomValue)**
---
--- Changes the custom value of the actor's target/tracer.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_RelayCustomValue(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SetCustomValue)**
---
--- Changes the actor's custom value.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SetCustomValue(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_UseCusValMemo)**
---
--- Saves or reloads the actor's custom value.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_UseCusValMemo(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CheckBuddy)**
---
--- Checks if actor's "buddy" has at least one hit point left.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CheckBuddy(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FaceTarget)**
---
--- Makes the actor face its target.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FaceTarget(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FaceTracer)**
---
--- Makes the actor face its tracer.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FaceTracer(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_InfoState)**
---
--- Switches to an anchor state.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_InfoState(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Repeat)**
---
--- Returns to a specified state a certain number of times. Useful for looping animations.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Repeat(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FlickyAim)**
---
--- Aims the actor towards a general angle and distance from its target.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FlickyAim(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FlickySpawn)**
---
--- Spawns a flicky from the actor.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FlickySpawn(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FlickyCenter)**
---
--- Handles flicky setup when used in conjunction with a placed Thing.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FlickyCenter(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FlickyFly)**
---
--- Boosts the actor through the air (or water) towards its target.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FlickyFly(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FlickySoar)**
---
--- A version of A_FlickyFly meant for the Puffin.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FlickySoar(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FlickyCoast)**
---
--- Slows down the actor's movement, and changes its state.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FlickyCoast(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FlickyHop)**
---
--- Makes the actor hop upon reaching the ground.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FlickyHop(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FlickyFlounder)**
---
--- Makes the actor flop about on the floor.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FlickyFlounder(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FlickyCheck)**
---
--- Checks the actor's airtime and vertical momentum, and changes its state accordingly.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FlickyCheck(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FlickyHeightCheck)**
---
--- Checks the actor's height against its target, and changes its state accordingly.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FlickyHeightCheck(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FlickyFlutter)**
---
--- Slows down the actor and cuts its falling speed.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FlickyFlutter(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Explode)**
---
--- Damages Objects within range of the actor.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Explode(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Fall)**
---
--- Changes the actor's flags. Used by players who have died.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Fall(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_ForceWin)**
---
--- If there are any living players, makes them finish the level, as if they stepped into the exit sector.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_ForceWin(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_GoopSplat)**
---
--- Changes the actor's flags. Used by the goop projectiles of v2.0's Brak Eggman.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_GoopSplat(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_OverlayThink)**
---
--- Thinker for overlay-like Objects. Moves the actor to the position of its target.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_OverlayThink(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SetSolidSteam)**
---
--- Makes Gas Jets solid.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SetSolidSteam(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SignPlayer)**
---
--- Changes the state and skin color of the Level End Sign.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SignPlayer(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SlingAppear)**
---
--- Used to make the Hidden Chain appear in the map.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SlingAppear(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SpikeRetract)**
---
--- Toggles the solidity of spikes.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SpikeRetract(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_ToggleFlameJet)**
---
--- Turns flame jets on and off.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_ToggleFlameJet(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_UnsetSolidSteam)**
---
--- Makes solid Gas Jets intangible again.
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_UnsetSolidSteam(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_ArrowBonks)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_ArrowBonks(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss3ShockThink)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss3ShockThink(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss5BombExplode)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss5BombExplode(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss5Calm)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss5Calm(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss5CheckFalling)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss5CheckFalling(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss5CheckOnGround)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss5CheckOnGround(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss5ExtraRepeat)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss5ExtraRepeat(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss5FindWaypoint)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss5FindWaypoint(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss5Jump)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss5Jump(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss5MakeItRain)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss5MakeItRain(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss5MakeJunk)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss5MakeJunk(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Boss5PinchShot)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Boss5PinchShot(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_ChangeHeight)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_ChangeHeight(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_ChangeRollAngleAbsolute)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_ChangeRollAngleAbsolute(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_ChangeRollAngleRelative)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_ChangeRollAngleRelative(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CheckFlags2)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CheckFlags2(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CrushclawAim)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CrushclawAim(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CrushclawLaunch)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CrushclawLaunch(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CrushstaceanPunch)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CrushstaceanPunch(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CrushstaceanWalk)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CrushstaceanWalk(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_CryingToMomma)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_CryingToMomma(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_DebrisRandom)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_DebrisRandom(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_DoNPCPain)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_DoNPCPain(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_DoNPCSkid)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_DoNPCSkid(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_DragonbomberSpawn)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_DragonbomberSpawn(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_DragonSegment)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_DragonSegment(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_DragonWing)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_DragonWing(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_DustDevilThink)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_DustDevilThink(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_Dye)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_Dye(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FaceStabHurl)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FaceStabHurl(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FaceStabMiss)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FaceStabMiss(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FaceStabRev)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FaceStabRev(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FadeOverlay)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FadeOverlay(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FallingLavaCheck)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FallingLavaCheck(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FireShrink)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FireShrink(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_FlameParticle)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_FlameParticle(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_GoldMonitorPop)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_GoldMonitorPop(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_GoldMonitorRestore)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_GoldMonitorRestore(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_GoldMonitorSparkle)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_GoldMonitorSparkle(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_HoodFall)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_HoodFall(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_HoodFire)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_HoodFire(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_KillSegments)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_KillSegments(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_LavafallLava)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_LavafallLava(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_LavafallRocks)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_LavafallRocks(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_LightBeamReset)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_LightBeamReset(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_LookForBetter)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_LookForBetter(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_MinecartSparkThink)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_MinecartSparkThink(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_MineExplode)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_MineExplode(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_MineRange)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_MineRange(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_ModuloToState)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_ModuloToState(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_MultiShotDist)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_MultiShotDist(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_ParentTriesToSleep)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_ParentTriesToSleep(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_PrepareRepeat)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_PrepareRepeat(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_PterabyteHover)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_PterabyteHover(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_RollAngle)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_RollAngle(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_RolloutRock)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_RolloutRock(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_RolloutSpawn)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_RolloutSpawn(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SaloonDoorSpawn)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SaloonDoorSpawn(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SharpDecel)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SharpDecel(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SignSpin)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SignSpin(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SnapperSpawn)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SnapperSpawn(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SnapperThinker)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SnapperThinker(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SpawnParticleRelative)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SpawnParticleRelative(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_SpawnPterabytes)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_SpawnPterabytes(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_StateRangeByAngle)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_StateRangeByAngle(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_StateRangeByParameter)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_StateRangeByParameter(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_StatueBurst)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_StatueBurst(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_TNTExplode)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_TNTExplode(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_TrainCameo)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_TrainCameo(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_TrainCameo2)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_TrainCameo2(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_VultureBlast)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_VultureBlast(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_VultureFly)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_VultureFly(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_VultureHover)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_VultureHover(actor, var1, var2) end

---
---**[View on wiki](https://wiki.srb2.org/wiki/A_WhoCaresIfYourSonIsABee)**
---
---@param actor mobj_t
---@param var1 INT32
---@param var2 INT32
function A_WhoCaresIfYourSonIsABee(actor, var1, var2) end


---Empty state, automatically removes the Object that uses it from existence
S_NULL = 0
---Used as a fallback when a state cannot be found, displays a warning graphic
S_UNKNOWN = 1
---Generic state with indeterminate duration for invisible Objects
S_INVISIBLE = 2
---Auxiliary state, automatically redirects the Object that uses it to its `SpawnState`
S_SPAWNSTATE = 3
---Automatically redirects the Object that uses it to its `SeeState`
S_SEESTATE = 4
---Automatically redirects the Object that uses it to its `MeleeState`
S_MELEESTATE = 5
---Automatically redirects the Object that uses it to its `MissileState`
S_MISSILESTATE = 6
---Automatically redirects the Object that uses it to its `DeathState`
S_DEATHSTATE = 7
---Automatically redirects the Object that uses it to its `XDeathState`
S_XDEATHSTATE = 8
---Automatically redirects the Object that uses it to its `RaiseState`
S_RAISESTATE = 9
S_THOK = 10
S_PLAY_STND = 11
S_PLAY_WAIT = 12
S_PLAY_WALK = 13
S_PLAY_SKID = 14
S_PLAY_RUN = 15
S_PLAY_DASH = 16
S_PLAY_PAIN = 17
S_PLAY_STUN = 18
S_PLAY_DEAD = 19
S_PLAY_DRWN = 20
S_PLAY_ROLL = 21
S_PLAY_GASP = 22
S_PLAY_JUMP = 23
S_PLAY_SPRING = 24
S_PLAY_FALL = 25
S_PLAY_EDGE = 26
S_PLAY_RIDE = 27
S_PLAY_SPINDASH = 28
S_PLAY_FLY = 29
S_PLAY_SWIM = 30
S_PLAY_FLY_TIRED = 31
S_PLAY_GLIDE = 32
S_PLAY_GLIDE_LANDING = 33
S_PLAY_CLING = 34
S_PLAY_CLIMB = 35
S_PLAY_FLOAT = 36
S_PLAY_FLOAT_RUN = 37
S_PLAY_BOUNCE = 38
S_PLAY_BOUNCE_LANDING = 39
S_PLAY_FIRE = 40
S_PLAY_FIRE_FINISH = 41
S_PLAY_TWINSPIN = 42
S_PLAY_MELEE = 43
S_PLAY_MELEE_FINISH = 44
S_PLAY_MELEE_LANDING = 45
S_PLAY_SUPER_TRANS1 = 46
S_PLAY_SUPER_TRANS2 = 47
S_PLAY_SUPER_TRANS3 = 48
S_PLAY_SUPER_TRANS4 = 49
S_PLAY_SUPER_TRANS5 = 50
S_PLAY_SUPER_TRANS6 = 51
S_OBJPLACE_DUMMY = 52
S_PLAY_BOX1 = 53
S_PLAY_BOX2 = 54
S_PLAY_ICON1 = 55
S_PLAY_ICON2 = 56
S_PLAY_ICON3 = 57
S_PLAY_SIGN = 58
S_PLAY_NIGHTS_TRANS1 = 59
S_PLAY_NIGHTS_TRANS2 = 60
S_PLAY_NIGHTS_TRANS3 = 61
S_PLAY_NIGHTS_TRANS4 = 62
S_PLAY_NIGHTS_TRANS5 = 63
S_PLAY_NIGHTS_TRANS6 = 64
S_PLAY_NIGHTS_STAND = 65
S_PLAY_NIGHTS_FLOAT = 66
S_PLAY_NIGHTS_FLY = 67
S_PLAY_NIGHTS_DRILL = 68
S_PLAY_NIGHTS_STUN = 69
S_PLAY_NIGHTS_PULL = 70
S_PLAY_NIGHTS_ATTACK = 71
S_TAILSOVERLAY_STAND = 72
S_TAILSOVERLAY_0DEGREES = 73
S_TAILSOVERLAY_PLUS30DEGREES = 74
S_TAILSOVERLAY_PLUS60DEGREES = 75
S_TAILSOVERLAY_MINUS30DEGREES = 76
S_TAILSOVERLAY_MINUS60DEGREES = 77
S_TAILSOVERLAY_RUN = 78
S_TAILSOVERLAY_FLY = 79
S_TAILSOVERLAY_TIRE = 80
S_TAILSOVERLAY_PAIN = 81
S_TAILSOVERLAY_GASP = 82
S_TAILSOVERLAY_EDGE = 83
S_TAILSOVERLAY_DASH = 84
S_JETFUMEFLASH = 85
S_POSS_STND = 86
S_POSS_RUN1 = 87
S_POSS_RUN2 = 88
S_POSS_RUN3 = 89
S_POSS_RUN4 = 90
S_POSS_RUN5 = 91
S_POSS_RUN6 = 92
S_SPOS_STND = 93
S_SPOS_RUN1 = 94
S_SPOS_RUN2 = 95
S_SPOS_RUN3 = 96
S_SPOS_RUN4 = 97
S_SPOS_RUN5 = 98
S_SPOS_RUN6 = 99
S_FISH1 = 100
S_FISH2 = 101
S_FISH3 = 102
S_FISH4 = 103
S_BUZZLOOK1 = 104
S_BUZZLOOK2 = 105
S_BUZZFLY1 = 106
S_BUZZFLY2 = 107
S_RBUZZLOOK1 = 108
S_RBUZZLOOK2 = 109
S_RBUZZFLY1 = 110
S_RBUZZFLY2 = 111
S_JETBLOOK1 = 112
S_JETBLOOK2 = 113
S_JETBZOOM1 = 114
S_JETBZOOM2 = 115
S_JETGLOOK1 = 116
S_JETGLOOK2 = 117
S_JETGZOOM1 = 118
S_JETGZOOM2 = 119
S_JETGSHOOT1 = 120
S_JETGSHOOT2 = 121
S_CCOMMAND1 = 122
S_CCOMMAND2 = 123
S_CCOMMAND3 = 124
S_CCOMMAND4 = 125
S_DETON1 = 126
S_DETON2 = 127
S_DETON3 = 128
S_DETON4 = 129
S_DETON5 = 130
S_DETON6 = 131
S_DETON7 = 132
S_DETON8 = 133
S_DETON9 = 134
S_DETON10 = 135
S_DETON11 = 136
S_DETON12 = 137
S_DETON13 = 138
S_DETON14 = 139
S_DETON15 = 140
S_SKIM1 = 141
S_SKIM2 = 142
S_SKIM3 = 143
S_SKIM4 = 144
S_TURRET = 145
S_TURRETFIRE = 146
S_TURRETSHOCK1 = 147
S_TURRETSHOCK2 = 148
S_TURRETSHOCK3 = 149
S_TURRETSHOCK4 = 150
S_TURRETSHOCK5 = 151
S_TURRETSHOCK6 = 152
S_TURRETSHOCK7 = 153
S_TURRETSHOCK8 = 154
S_TURRETSHOCK9 = 155
S_TURRETLOOK = 156
S_TURRETSEE = 157
S_TURRETPOPUP1 = 158
S_TURRETPOPUP2 = 159
S_TURRETPOPUP3 = 160
S_TURRETPOPUP4 = 161
S_TURRETPOPUP5 = 162
S_TURRETPOPUP6 = 163
S_TURRETPOPUP7 = 164
S_TURRETPOPUP8 = 165
S_TURRETSHOOT = 166
S_TURRETPOPDOWN1 = 167
S_TURRETPOPDOWN2 = 168
S_TURRETPOPDOWN3 = 169
S_TURRETPOPDOWN4 = 170
S_TURRETPOPDOWN5 = 171
S_TURRETPOPDOWN6 = 172
S_TURRETPOPDOWN7 = 173
S_TURRETPOPDOWN8 = 174
S_SPINCUSHION_LOOK = 175
S_SPINCUSHION_CHASE1 = 176
S_SPINCUSHION_CHASE2 = 177
S_SPINCUSHION_CHASE3 = 178
S_SPINCUSHION_CHASE4 = 179
S_SPINCUSHION_AIM1 = 180
S_SPINCUSHION_AIM2 = 181
S_SPINCUSHION_AIM3 = 182
S_SPINCUSHION_AIM4 = 183
S_SPINCUSHION_AIM5 = 184
S_SPINCUSHION_SPIN1 = 185
S_SPINCUSHION_SPIN2 = 186
S_SPINCUSHION_SPIN3 = 187
S_SPINCUSHION_SPIN4 = 188
S_SPINCUSHION_STOP1 = 189
S_SPINCUSHION_STOP2 = 190
S_SPINCUSHION_STOP3 = 191
S_SPINCUSHION_STOP4 = 192
S_CRUSHSTACEAN_ROAM1 = 193
S_CRUSHSTACEAN_ROAM2 = 194
S_CRUSHSTACEAN_ROAM3 = 195
S_CRUSHSTACEAN_ROAM4 = 196
S_CRUSHSTACEAN_ROAMPAUSE = 197
S_CRUSHSTACEAN_PUNCH1 = 198
S_CRUSHSTACEAN_PUNCH2 = 199
S_CRUSHCLAW_AIM = 200
S_CRUSHCLAW_OUT = 201
S_CRUSHCLAW_STAY = 202
S_CRUSHCLAW_IN = 203
S_CRUSHCLAW_WAIT = 204
S_CRUSHCHAIN = 205
S_BANPYURA_ROAM1 = 206
S_BANPYURA_ROAM2 = 207
S_BANPYURA_ROAM3 = 208
S_BANPYURA_ROAM4 = 209
S_BANPYURA_ROAMPAUSE = 210
S_CDIAG1 = 211
S_CDIAG2 = 212
S_CDIAG3 = 213
S_CDIAG4 = 214
S_CDIAG5 = 215
S_CDIAG6 = 216
S_CDIAG7 = 217
S_CDIAG8 = 218
S_JETJAW_ROAM1 = 219
S_JETJAW_ROAM2 = 220
S_JETJAW_ROAM3 = 221
S_JETJAW_ROAM4 = 222
S_JETJAW_ROAM5 = 223
S_JETJAW_ROAM6 = 224
S_JETJAW_ROAM7 = 225
S_JETJAW_ROAM8 = 226
S_JETJAW_CHOMP1 = 227
S_JETJAW_CHOMP2 = 228
S_JETJAW_CHOMP3 = 229
S_JETJAW_CHOMP4 = 230
S_JETJAW_CHOMP5 = 231
S_JETJAW_CHOMP6 = 232
S_JETJAW_CHOMP7 = 233
S_JETJAW_CHOMP8 = 234
S_JETJAW_CHOMP9 = 235
S_JETJAW_CHOMP10 = 236
S_JETJAW_CHOMP11 = 237
S_JETJAW_CHOMP12 = 238
S_JETJAW_CHOMP13 = 239
S_JETJAW_CHOMP14 = 240
S_JETJAW_CHOMP15 = 241
S_JETJAW_CHOMP16 = 242
S_JETJAW_SOUND = 243
S_SNAILER1 = 244
S_SNAILER_FLICKY = 245
S_VULTURE_STND = 246
S_VULTURE_DRIFT = 247
S_VULTURE_ZOOM1 = 248
S_VULTURE_ZOOM2 = 249
S_VULTURE_STUNNED = 250
S_POINTY1 = 251
S_POINTYBALL1 = 252
S_ROBOHOOD_LOOK = 253
S_ROBOHOOD_STAND = 254
S_ROBOHOOD_FIRE1 = 255
S_ROBOHOOD_FIRE2 = 256
S_ROBOHOOD_JUMP1 = 257
S_ROBOHOOD_JUMP2 = 258
S_ROBOHOOD_JUMP3 = 259
S_FACESTABBER_STND1 = 260
S_FACESTABBER_STND2 = 261
S_FACESTABBER_STND3 = 262
S_FACESTABBER_STND4 = 263
S_FACESTABBER_STND5 = 264
S_FACESTABBER_STND6 = 265
S_FACESTABBER_CHARGE1 = 266
S_FACESTABBER_CHARGE2 = 267
S_FACESTABBER_CHARGE3 = 268
S_FACESTABBER_CHARGE4 = 269
S_FACESTABBER_PAIN = 270
S_FACESTABBER_DIE1 = 271
S_FACESTABBER_DIE2 = 272
S_FACESTABBER_DIE3 = 273
S_FACESTABBERSPEAR = 274
S_EGGGUARD_STND = 275
S_EGGGUARD_WALK1 = 276
S_EGGGUARD_WALK2 = 277
S_EGGGUARD_WALK3 = 278
S_EGGGUARD_WALK4 = 279
S_EGGGUARD_MAD1 = 280
S_EGGGUARD_MAD2 = 281
S_EGGGUARD_MAD3 = 282
S_EGGGUARD_RUN1 = 283
S_EGGGUARD_RUN2 = 284
S_EGGGUARD_RUN3 = 285
S_EGGGUARD_RUN4 = 286
S_EGGSHIELD = 287
S_EGGSHIELDBREAK = 288
S_SNAPPER_SPAWN = 289
S_SNAPPER_SPAWN2 = 290
S_GSNAPPER_STND = 291
S_GSNAPPER1 = 292
S_GSNAPPER2 = 293
S_GSNAPPER3 = 294
S_GSNAPPER4 = 295
S_SNAPPER_XPLD = 296
S_SNAPPER_LEG = 297
S_SNAPPER_LEGRAISE = 298
S_SNAPPER_HEAD = 299
S_MINUS_INIT = 300
S_MINUS_STND = 301
S_MINUS_DIGGING1 = 302
S_MINUS_DIGGING2 = 303
S_MINUS_DIGGING3 = 304
S_MINUS_DIGGING4 = 305
S_MINUS_BURST0 = 306
S_MINUS_BURST1 = 307
S_MINUS_BURST2 = 308
S_MINUS_BURST3 = 309
S_MINUS_BURST4 = 310
S_MINUS_BURST5 = 311
S_MINUS_POPUP = 312
S_MINUS_AERIAL1 = 313
S_MINUS_AERIAL2 = 314
S_MINUS_AERIAL3 = 315
S_MINUS_AERIAL4 = 316
S_MINUSDIRT1 = 317
S_MINUSDIRT2 = 318
S_MINUSDIRT3 = 319
S_MINUSDIRT4 = 320
S_MINUSDIRT5 = 321
S_MINUSDIRT6 = 322
S_MINUSDIRT7 = 323
S_SSHELL_STND = 324
S_SSHELL_RUN1 = 325
S_SSHELL_RUN2 = 326
S_SSHELL_RUN3 = 327
S_SSHELL_RUN4 = 328
S_SSHELL_SPRING1 = 329
S_SSHELL_SPRING2 = 330
S_SSHELL_SPRING3 = 331
S_SSHELL_SPRING4 = 332
S_YSHELL_STND = 333
S_YSHELL_RUN1 = 334
S_YSHELL_RUN2 = 335
S_YSHELL_RUN3 = 336
S_YSHELL_RUN4 = 337
S_YSHELL_SPRING1 = 338
S_YSHELL_SPRING2 = 339
S_YSHELL_SPRING3 = 340
S_YSHELL_SPRING4 = 341
S_UNIDUS_STND = 342
S_UNIDUS_RUN = 343
S_UNIDUS_BALL = 344
S_CANARIVORE_LOOK = 345
S_CANARIVORE_AWAKEN1 = 346
S_CANARIVORE_AWAKEN2 = 347
S_CANARIVORE_AWAKEN3 = 348
S_CANARIVORE_GAS1 = 349
S_CANARIVORE_GAS2 = 350
S_CANARIVORE_GAS3 = 351
S_CANARIVORE_GAS4 = 352
S_CANARIVORE_GAS5 = 353
S_CANARIVORE_GASREPEAT = 354
S_CANARIVORE_CLOSE1 = 355
S_CANARIVORE_CLOSE2 = 356
S_CANARIVOREGAS_1 = 357
S_CANARIVOREGAS_2 = 358
S_CANARIVOREGAS_3 = 359
S_CANARIVOREGAS_4 = 360
S_CANARIVOREGAS_5 = 361
S_CANARIVOREGAS_6 = 362
S_CANARIVOREGAS_7 = 363
S_CANARIVOREGAS_8 = 364
S_PYREFLY_FLY = 365
S_PYREFLY_BURN = 366
S_PYREFIRE1 = 367
S_PYREFIRE2 = 368
S_PTERABYTESPAWNER = 369
S_PTERABYTEWAYPOINT = 370
S_PTERABYTE_FLY1 = 371
S_PTERABYTE_FLY2 = 372
S_PTERABYTE_FLY3 = 373
S_PTERABYTE_FLY4 = 374
S_PTERABYTE_SWOOPDOWN = 375
S_PTERABYTE_SWOOPUP = 376
S_DRAGONBOMBER = 377
S_DRAGONWING1 = 378
S_DRAGONWING2 = 379
S_DRAGONWING3 = 380
S_DRAGONWING4 = 381
S_DRAGONTAIL_LOADED = 382
S_DRAGONTAIL_EMPTY = 383
S_DRAGONTAIL_EMPTYLOOP = 384
S_DRAGONTAIL_RELOAD = 385
S_DRAGONMINE = 386
S_DRAGONMINE_LAND1 = 387
S_DRAGONMINE_LAND2 = 388
S_DRAGONMINE_SLOWFLASH1 = 389
S_DRAGONMINE_SLOWFLASH2 = 390
S_DRAGONMINE_SLOWLOOP = 391
S_DRAGONMINE_FASTFLASH1 = 392
S_DRAGONMINE_FASTFLASH2 = 393
S_DRAGONMINE_FASTLOOP = 394
S_BOSSEXPLODE = 395
S_SONIC3KBOSSEXPLOSION1 = 396
S_SONIC3KBOSSEXPLOSION2 = 397
S_SONIC3KBOSSEXPLOSION3 = 398
S_SONIC3KBOSSEXPLOSION4 = 399
S_SONIC3KBOSSEXPLOSION5 = 400
S_SONIC3KBOSSEXPLOSION6 = 401
S_JETFUME1 = 402
S_EGGMOBILE_STND = 403
S_EGGMOBILE_ROFL = 404
S_EGGMOBILE_LATK1 = 405
S_EGGMOBILE_LATK2 = 406
S_EGGMOBILE_LATK3 = 407
S_EGGMOBILE_LATK4 = 408
S_EGGMOBILE_LATK5 = 409
S_EGGMOBILE_LATK6 = 410
S_EGGMOBILE_LATK7 = 411
S_EGGMOBILE_LATK8 = 412
S_EGGMOBILE_LATK9 = 413
S_EGGMOBILE_RATK1 = 414
S_EGGMOBILE_RATK2 = 415
S_EGGMOBILE_RATK3 = 416
S_EGGMOBILE_RATK4 = 417
S_EGGMOBILE_RATK5 = 418
S_EGGMOBILE_RATK6 = 419
S_EGGMOBILE_RATK7 = 420
S_EGGMOBILE_RATK8 = 421
S_EGGMOBILE_RATK9 = 422
S_EGGMOBILE_PANIC1 = 423
S_EGGMOBILE_PANIC2 = 424
S_EGGMOBILE_PANIC3 = 425
S_EGGMOBILE_PANIC4 = 426
S_EGGMOBILE_PANIC5 = 427
S_EGGMOBILE_PANIC6 = 428
S_EGGMOBILE_PANIC7 = 429
S_EGGMOBILE_PANIC8 = 430
S_EGGMOBILE_PANIC9 = 431
S_EGGMOBILE_PANIC10 = 432
S_EGGMOBILE_PANIC11 = 433
S_EGGMOBILE_PANIC12 = 434
S_EGGMOBILE_PANIC13 = 435
S_EGGMOBILE_PANIC14 = 436
S_EGGMOBILE_PANIC15 = 437
S_EGGMOBILE_PAIN = 438
S_EGGMOBILE_PAIN2 = 439
S_EGGMOBILE_DIE1 = 440
S_EGGMOBILE_DIE2 = 441
S_EGGMOBILE_DIE3 = 442
S_EGGMOBILE_DIE4 = 443
S_EGGMOBILE_FLEE1 = 444
S_EGGMOBILE_FLEE2 = 445
S_EGGMOBILE_BALL = 446
S_EGGMOBILE_TARGET = 447
S_BOSSEGLZ1 = 448
S_BOSSEGLZ2 = 449
S_EGGMOBILE2_STND = 450
S_EGGMOBILE2_POGO1 = 451
S_EGGMOBILE2_POGO2 = 452
S_EGGMOBILE2_POGO3 = 453
S_EGGMOBILE2_POGO4 = 454
S_EGGMOBILE2_POGO5 = 455
S_EGGMOBILE2_POGO6 = 456
S_EGGMOBILE2_POGO7 = 457
S_EGGMOBILE2_PAIN = 458
S_EGGMOBILE2_PAIN2 = 459
S_EGGMOBILE2_DIE1 = 460
S_EGGMOBILE2_DIE2 = 461
S_EGGMOBILE2_DIE3 = 462
S_EGGMOBILE2_DIE4 = 463
S_EGGMOBILE2_FLEE1 = 464
S_EGGMOBILE2_FLEE2 = 465
S_BOSSTANK1 = 466
S_BOSSTANK2 = 467
S_BOSSSPIGOT = 468
S_GOOP1 = 469
S_GOOP2 = 470
S_GOOP3 = 471
S_GOOPTRAIL = 472
S_EGGMOBILE3_STND = 473
S_EGGMOBILE3_SHOCK = 474
S_EGGMOBILE3_ATK1 = 475
S_EGGMOBILE3_ATK2 = 476
S_EGGMOBILE3_ATK3A = 477
S_EGGMOBILE3_ATK3B = 478
S_EGGMOBILE3_ATK3C = 479
S_EGGMOBILE3_ATK3D = 480
S_EGGMOBILE3_ATK4 = 481
S_EGGMOBILE3_ATK5 = 482
S_EGGMOBILE3_ROFL = 483
S_EGGMOBILE3_PAIN = 484
S_EGGMOBILE3_PAIN2 = 485
S_EGGMOBILE3_DIE1 = 486
S_EGGMOBILE3_DIE2 = 487
S_EGGMOBILE3_DIE3 = 488
S_EGGMOBILE3_DIE4 = 489
S_EGGMOBILE3_FLEE1 = 490
S_EGGMOBILE3_FLEE2 = 491
S_FAKEMOBILE_INIT = 492
S_FAKEMOBILE = 493
S_FAKEMOBILE_ATK1 = 494
S_FAKEMOBILE_ATK2 = 495
S_FAKEMOBILE_ATK3A = 496
S_FAKEMOBILE_ATK3B = 497
S_FAKEMOBILE_ATK3C = 498
S_FAKEMOBILE_ATK3D = 499
S_FAKEMOBILE_DIE1 = 500
S_FAKEMOBILE_DIE2 = 501
S_BOSSSEBH1 = 502
S_BOSSSEBH2 = 503
S_SHOCKWAVE1 = 504
S_SHOCKWAVE2 = 505
S_EGGMOBILE4_STND = 506
S_EGGMOBILE4_LATK1 = 507
S_EGGMOBILE4_LATK2 = 508
S_EGGMOBILE4_LATK3 = 509
S_EGGMOBILE4_LATK4 = 510
S_EGGMOBILE4_LATK5 = 511
S_EGGMOBILE4_LATK6 = 512
S_EGGMOBILE4_RATK1 = 513
S_EGGMOBILE4_RATK2 = 514
S_EGGMOBILE4_RATK3 = 515
S_EGGMOBILE4_RATK4 = 516
S_EGGMOBILE4_RATK5 = 517
S_EGGMOBILE4_RATK6 = 518
S_EGGMOBILE4_RAISE1 = 519
S_EGGMOBILE4_RAISE2 = 520
S_EGGMOBILE4_PAIN1 = 521
S_EGGMOBILE4_PAIN2 = 522
S_EGGMOBILE4_DIE1 = 523
S_EGGMOBILE4_DIE2 = 524
S_EGGMOBILE4_DIE3 = 525
S_EGGMOBILE4_DIE4 = 526
S_EGGMOBILE4_FLEE1 = 527
S_EGGMOBILE4_FLEE2 = 528
S_EGGMOBILE4_MACE = 529
S_EGGMOBILE4_MACE_DIE1 = 530
S_EGGMOBILE4_MACE_DIE2 = 531
S_EGGMOBILE4_MACE_DIE3 = 532
S_JETFLAME = 533
S_EGGROBO1_STND = 534
S_EGGROBO1_BSLAP1 = 535
S_EGGROBO1_BSLAP2 = 536
S_EGGROBO1_PISSED = 537
S_EGGROBOJET = 538
S_FANG_SETUP = 539
S_FANG_INTRO0 = 540
S_FANG_INTRO1 = 541
S_FANG_INTRO2 = 542
S_FANG_INTRO3 = 543
S_FANG_INTRO4 = 544
S_FANG_INTRO5 = 545
S_FANG_INTRO6 = 546
S_FANG_INTRO7 = 547
S_FANG_INTRO8 = 548
S_FANG_INTRO9 = 549
S_FANG_INTRO10 = 550
S_FANG_INTRO11 = 551
S_FANG_INTRO12 = 552
S_FANG_CLONE1 = 553
S_FANG_CLONE2 = 554
S_FANG_CLONE3 = 555
S_FANG_CLONE4 = 556
S_FANG_IDLE0 = 557
S_FANG_IDLE1 = 558
S_FANG_IDLE2 = 559
S_FANG_IDLE3 = 560
S_FANG_IDLE4 = 561
S_FANG_IDLE5 = 562
S_FANG_IDLE6 = 563
S_FANG_IDLE7 = 564
S_FANG_IDLE8 = 565
S_FANG_PAIN1 = 566
S_FANG_PAIN2 = 567
S_FANG_PATHINGSTART1 = 568
S_FANG_PATHINGSTART2 = 569
S_FANG_PATHING = 570
S_FANG_BOUNCE1 = 571
S_FANG_BOUNCE2 = 572
S_FANG_BOUNCE3 = 573
S_FANG_BOUNCE4 = 574
S_FANG_FALL1 = 575
S_FANG_FALL2 = 576
S_FANG_CHECKPATH1 = 577
S_FANG_CHECKPATH2 = 578
S_FANG_PATHINGCONT1 = 579
S_FANG_PATHINGCONT2 = 580
S_FANG_PATHINGCONT3 = 581
S_FANG_SKID1 = 582
S_FANG_SKID2 = 583
S_FANG_SKID3 = 584
S_FANG_CHOOSEATTACK = 585
S_FANG_FIRESTART1 = 586
S_FANG_FIRESTART2 = 587
S_FANG_FIRE1 = 588
S_FANG_FIRE2 = 589
S_FANG_FIRE3 = 590
S_FANG_FIRE4 = 591
S_FANG_FIREREPEAT = 592
S_FANG_LOBSHOT0 = 593
S_FANG_LOBSHOT1 = 594
S_FANG_LOBSHOT2 = 595
S_FANG_WAIT1 = 596
S_FANG_WAIT2 = 597
S_FANG_WALLHIT = 598
S_FANG_PINCHPATHINGSTART1 = 599
S_FANG_PINCHPATHINGSTART2 = 600
S_FANG_PINCHPATHING = 601
S_FANG_PINCHBOUNCE0 = 602
S_FANG_PINCHBOUNCE1 = 603
S_FANG_PINCHBOUNCE2 = 604
S_FANG_PINCHBOUNCE3 = 605
S_FANG_PINCHBOUNCE4 = 606
S_FANG_PINCHFALL0 = 607
S_FANG_PINCHFALL1 = 608
S_FANG_PINCHFALL2 = 609
S_FANG_PINCHSKID1 = 610
S_FANG_PINCHSKID2 = 611
S_FANG_PINCHLOBSHOT0 = 612
S_FANG_PINCHLOBSHOT1 = 613
S_FANG_PINCHLOBSHOT2 = 614
S_FANG_PINCHLOBSHOT3 = 615
S_FANG_PINCHLOBSHOT4 = 616
S_FANG_DIE1 = 617
S_FANG_DIE2 = 618
S_FANG_DIE3 = 619
S_FANG_DIE4 = 620
S_FANG_DIE5 = 621
S_FANG_DIE6 = 622
S_FANG_DIE7 = 623
S_FANG_DIE8 = 624
S_FANG_FLEEPATHING1 = 625
S_FANG_FLEEPATHING2 = 626
S_FANG_FLEEBOUNCE1 = 627
S_FANG_FLEEBOUNCE2 = 628
S_FANG_KO = 629
S_BROKENROBOTRANDOM = 630
S_BROKENROBOTA = 631
S_BROKENROBOTB = 632
S_BROKENROBOTC = 633
S_BROKENROBOTD = 634
S_BROKENROBOTE = 635
S_BROKENROBOTF = 636
S_ALART1 = 637
S_ALART2 = 638
S_VWREF = 639
S_VWREB = 640
S_PROJECTORLIGHT1 = 641
S_PROJECTORLIGHT2 = 642
S_PROJECTORLIGHT3 = 643
S_PROJECTORLIGHT4 = 644
S_PROJECTORLIGHT5 = 645
S_FBOMB1 = 646
S_FBOMB2 = 647
S_FBOMB_EXPL1 = 648
S_FBOMB_EXPL2 = 649
S_FBOMB_EXPL3 = 650
S_FBOMB_EXPL4 = 651
S_FBOMB_EXPL5 = 652
S_FBOMB_EXPL6 = 653
S_TNTDUST_1 = 654
S_TNTDUST_2 = 655
S_TNTDUST_3 = 656
S_TNTDUST_4 = 657
S_TNTDUST_5 = 658
S_TNTDUST_6 = 659
S_TNTDUST_7 = 660
S_TNTDUST_8 = 661
S_FSGNA = 662
S_FSGNB = 663
S_FSGNC = 664
S_FSGND = 665
S_BLACKEGG_STND = 666
S_BLACKEGG_STND2 = 667
S_BLACKEGG_WALK1 = 668
S_BLACKEGG_WALK2 = 669
S_BLACKEGG_WALK3 = 670
S_BLACKEGG_WALK4 = 671
S_BLACKEGG_WALK5 = 672
S_BLACKEGG_WALK6 = 673
S_BLACKEGG_SHOOT1 = 674
S_BLACKEGG_SHOOT2 = 675
S_BLACKEGG_PAIN1 = 676
S_BLACKEGG_PAIN2 = 677
S_BLACKEGG_PAIN3 = 678
S_BLACKEGG_PAIN4 = 679
S_BLACKEGG_PAIN5 = 680
S_BLACKEGG_PAIN6 = 681
S_BLACKEGG_PAIN7 = 682
S_BLACKEGG_PAIN8 = 683
S_BLACKEGG_PAIN9 = 684
S_BLACKEGG_PAIN10 = 685
S_BLACKEGG_PAIN11 = 686
S_BLACKEGG_PAIN12 = 687
S_BLACKEGG_PAIN13 = 688
S_BLACKEGG_PAIN14 = 689
S_BLACKEGG_PAIN15 = 690
S_BLACKEGG_PAIN16 = 691
S_BLACKEGG_PAIN17 = 692
S_BLACKEGG_PAIN18 = 693
S_BLACKEGG_PAIN19 = 694
S_BLACKEGG_PAIN20 = 695
S_BLACKEGG_PAIN21 = 696
S_BLACKEGG_PAIN22 = 697
S_BLACKEGG_PAIN23 = 698
S_BLACKEGG_PAIN24 = 699
S_BLACKEGG_PAIN25 = 700
S_BLACKEGG_PAIN26 = 701
S_BLACKEGG_PAIN27 = 702
S_BLACKEGG_PAIN28 = 703
S_BLACKEGG_PAIN29 = 704
S_BLACKEGG_PAIN30 = 705
S_BLACKEGG_PAIN31 = 706
S_BLACKEGG_PAIN32 = 707
S_BLACKEGG_PAIN33 = 708
S_BLACKEGG_PAIN34 = 709
S_BLACKEGG_PAIN35 = 710
S_BLACKEGG_HITFACE1 = 711
S_BLACKEGG_HITFACE2 = 712
S_BLACKEGG_HITFACE3 = 713
S_BLACKEGG_HITFACE4 = 714
S_BLACKEGG_DIE1 = 715
S_BLACKEGG_DIE2 = 716
S_BLACKEGG_DIE3 = 717
S_BLACKEGG_DIE4 = 718
S_BLACKEGG_DIE5 = 719
S_BLACKEGG_MISSILE1 = 720
S_BLACKEGG_MISSILE2 = 721
S_BLACKEGG_MISSILE3 = 722
S_BLACKEGG_GOOP = 723
S_BLACKEGG_JUMP1 = 724
S_BLACKEGG_JUMP2 = 725
S_BLACKEGG_DESTROYPLAT1 = 726
S_BLACKEGG_DESTROYPLAT2 = 727
S_BLACKEGG_DESTROYPLAT3 = 728
S_BLACKEGG_HELPER = 729
S_BLACKEGG_GOOP1 = 730
S_BLACKEGG_GOOP2 = 731
S_BLACKEGG_GOOP3 = 732
S_BLACKEGG_GOOP4 = 733
S_BLACKEGG_GOOP5 = 734
S_BLACKEGG_GOOP6 = 735
S_BLACKEGG_GOOP7 = 736
S_BLACKEGG_MISSILE = 737
S_CYBRAKDEMON_IDLE = 738
S_CYBRAKDEMON_WALK1 = 739
S_CYBRAKDEMON_WALK2 = 740
S_CYBRAKDEMON_WALK3 = 741
S_CYBRAKDEMON_WALK4 = 742
S_CYBRAKDEMON_WALK5 = 743
S_CYBRAKDEMON_WALK6 = 744
S_CYBRAKDEMON_CHOOSE_ATTACK1 = 745
S_CYBRAKDEMON_MISSILE_ATTACK1 = 746
S_CYBRAKDEMON_MISSILE_ATTACK2 = 747
S_CYBRAKDEMON_MISSILE_ATTACK3 = 748
S_CYBRAKDEMON_MISSILE_ATTACK4 = 749
S_CYBRAKDEMON_MISSILE_ATTACK5 = 750
S_CYBRAKDEMON_MISSILE_ATTACK6 = 751
S_CYBRAKDEMON_FLAME_ATTACK1 = 752
S_CYBRAKDEMON_FLAME_ATTACK2 = 753
S_CYBRAKDEMON_FLAME_ATTACK3 = 754
S_CYBRAKDEMON_FLAME_ATTACK4 = 755
S_CYBRAKDEMON_CHOOSE_ATTACK2 = 756
S_CYBRAKDEMON_VILE_ATTACK1 = 757
S_CYBRAKDEMON_VILE_ATTACK2 = 758
S_CYBRAKDEMON_VILE_ATTACK3 = 759
S_CYBRAKDEMON_VILE_ATTACK4 = 760
S_CYBRAKDEMON_VILE_ATTACK5 = 761
S_CYBRAKDEMON_VILE_ATTACK6 = 762
S_CYBRAKDEMON_NAPALM_ATTACK1 = 763
S_CYBRAKDEMON_NAPALM_ATTACK2 = 764
S_CYBRAKDEMON_NAPALM_ATTACK3 = 765
S_CYBRAKDEMON_FINISH_ATTACK1 = 766
S_CYBRAKDEMON_FINISH_ATTACK2 = 767
S_CYBRAKDEMON_PAIN1 = 768
S_CYBRAKDEMON_PAIN2 = 769
S_CYBRAKDEMON_PAIN3 = 770
S_CYBRAKDEMON_DIE1 = 771
S_CYBRAKDEMON_DIE2 = 772
S_CYBRAKDEMON_DIE3 = 773
S_CYBRAKDEMON_DIE4 = 774
S_CYBRAKDEMON_DIE5 = 775
S_CYBRAKDEMON_DIE6 = 776
S_CYBRAKDEMON_DIE7 = 777
S_CYBRAKDEMON_DIE8 = 778
S_CYBRAKDEMON_DEINVINCIBLERIZE = 779
S_CYBRAKDEMON_INVINCIBLERIZE = 780
S_CYBRAKDEMONMISSILE = 781
S_CYBRAKDEMONMISSILE_EXPLODE1 = 782
S_CYBRAKDEMONMISSILE_EXPLODE2 = 783
S_CYBRAKDEMONMISSILE_EXPLODE3 = 784
S_CYBRAKDEMONFLAMESHOT_FLY1 = 785
S_CYBRAKDEMONFLAMESHOT_FLY2 = 786
S_CYBRAKDEMONFLAMESHOT_FLY3 = 787
S_CYBRAKDEMONFLAMESHOT_DIE = 788
S_CYBRAKDEMONFLAMEREST = 789
S_CYBRAKDEMONELECTRICBARRIER_INIT1 = 790
S_CYBRAKDEMONELECTRICBARRIER_INIT2 = 791
S_CYBRAKDEMONELECTRICBARRIER_PLAYSOUND = 792
S_CYBRAKDEMONELECTRICBARRIER1 = 793
S_CYBRAKDEMONELECTRICBARRIER2 = 794
S_CYBRAKDEMONELECTRICBARRIER3 = 795
S_CYBRAKDEMONELECTRICBARRIER4 = 796
S_CYBRAKDEMONELECTRICBARRIER5 = 797
S_CYBRAKDEMONELECTRICBARRIER6 = 798
S_CYBRAKDEMONELECTRICBARRIER7 = 799
S_CYBRAKDEMONELECTRICBARRIER8 = 800
S_CYBRAKDEMONELECTRICBARRIER9 = 801
S_CYBRAKDEMONELECTRICBARRIER10 = 802
S_CYBRAKDEMONELECTRICBARRIER11 = 803
S_CYBRAKDEMONELECTRICBARRIER12 = 804
S_CYBRAKDEMONELECTRICBARRIER13 = 805
S_CYBRAKDEMONELECTRICBARRIER14 = 806
S_CYBRAKDEMONELECTRICBARRIER15 = 807
S_CYBRAKDEMONELECTRICBARRIER16 = 808
S_CYBRAKDEMONELECTRICBARRIER17 = 809
S_CYBRAKDEMONELECTRICBARRIER18 = 810
S_CYBRAKDEMONELECTRICBARRIER19 = 811
S_CYBRAKDEMONELECTRICBARRIER20 = 812
S_CYBRAKDEMONELECTRICBARRIER21 = 813
S_CYBRAKDEMONELECTRICBARRIER22 = 814
S_CYBRAKDEMONELECTRICBARRIER23 = 815
S_CYBRAKDEMONELECTRICBARRIER24 = 816
S_CYBRAKDEMONELECTRICBARRIER_DIE1 = 817
S_CYBRAKDEMONELECTRICBARRIER_DIE2 = 818
S_CYBRAKDEMONELECTRICBARRIER_DIE3 = 819
S_CYBRAKDEMONELECTRICBARRIER_SPARK_RANDOMCHECK = 820
S_CYBRAKDEMONELECTRICBARRIER_SPARK_RANDOMSUCCESS = 821
S_CYBRAKDEMONELECTRICBARRIER_SPARK_RANDOMCHOOSE = 822
S_CYBRAKDEMONELECTRICBARRIER_SPARK_RANDOM1 = 823
S_CYBRAKDEMONELECTRICBARRIER_SPARK_RANDOM2 = 824
S_CYBRAKDEMONELECTRICBARRIER_SPARK_RANDOM3 = 825
S_CYBRAKDEMONELECTRICBARRIER_SPARK_RANDOM4 = 826
S_CYBRAKDEMONELECTRICBARRIER_SPARK_RANDOM5 = 827
S_CYBRAKDEMONELECTRICBARRIER_SPARK_RANDOM6 = 828
S_CYBRAKDEMONELECTRICBARRIER_SPARK_RANDOM7 = 829
S_CYBRAKDEMONELECTRICBARRIER_SPARK_RANDOM8 = 830
S_CYBRAKDEMONELECTRICBARRIER_SPARK_RANDOM9 = 831
S_CYBRAKDEMONELECTRICBARRIER_SPARK_RANDOM10 = 832
S_CYBRAKDEMONELECTRICBARRIER_SPARK_RANDOM11 = 833
S_CYBRAKDEMONELECTRICBARRIER_SPARK_RANDOM12 = 834
S_CYBRAKDEMONELECTRICBARRIER_SPARK_RANDOMFAIL = 835
S_CYBRAKDEMONELECTRICBARRIER_SPARK_RANDOMLOOP = 836
S_CYBRAKDEMONELECTRICBARRIER_REVIVE1 = 837
S_CYBRAKDEMONELECTRICBARRIER_REVIVE2 = 838
S_CYBRAKDEMONELECTRICBARRIER_REVIVE3 = 839
S_CYBRAKDEMONTARGETRETICULE1 = 840
S_CYBRAKDEMONTARGETRETICULE2 = 841
S_CYBRAKDEMONTARGETRETICULE3 = 842
S_CYBRAKDEMONTARGETRETICULE4 = 843
S_CYBRAKDEMONTARGETRETICULE5 = 844
S_CYBRAKDEMONTARGETRETICULE6 = 845
S_CYBRAKDEMONTARGETRETICULE7 = 846
S_CYBRAKDEMONTARGETRETICULE8 = 847
S_CYBRAKDEMONTARGETRETICULE9 = 848
S_CYBRAKDEMONTARGETRETICULE10 = 849
S_CYBRAKDEMONTARGETRETICULE11 = 850
S_CYBRAKDEMONTARGETRETICULE12 = 851
S_CYBRAKDEMONTARGETRETICULE13 = 852
S_CYBRAKDEMONTARGETRETICULE14 = 853
S_CYBRAKDEMONTARGETDOT = 854
S_CYBRAKDEMONNAPALMBOMBLARGE_FLY1 = 855
S_CYBRAKDEMONNAPALMBOMBLARGE_FLY2 = 856
S_CYBRAKDEMONNAPALMBOMBLARGE_FLY3 = 857
S_CYBRAKDEMONNAPALMBOMBLARGE_FLY4 = 858
S_CYBRAKDEMONNAPALMBOMBLARGE_DIE1 = 859
S_CYBRAKDEMONNAPALMBOMBLARGE_DIE2 = 860
S_CYBRAKDEMONNAPALMBOMBLARGE_DIE3 = 861
S_CYBRAKDEMONNAPALMBOMBLARGE_DIE4 = 862
S_CYBRAKDEMONNAPALMBOMBSMALL = 863
S_CYBRAKDEMONNAPALMBOMBSMALL_DIE1 = 864
S_CYBRAKDEMONNAPALMBOMBSMALL_DIE2 = 865
S_CYBRAKDEMONNAPALMBOMBSMALL_DIE3 = 866
S_CYBRAKDEMONNAPALMBOMBSMALL_DIE4 = 867
S_CYBRAKDEMONNAPALMBOMBSMALL_DIE5 = 868
S_CYBRAKDEMONNAPALMFLAME_FLY1 = 869
S_CYBRAKDEMONNAPALMFLAME_FLY2 = 870
S_CYBRAKDEMONNAPALMFLAME_FLY3 = 871
S_CYBRAKDEMONNAPALMFLAME_FLY4 = 872
S_CYBRAKDEMONNAPALMFLAME_FLY5 = 873
S_CYBRAKDEMONNAPALMFLAME_FLY6 = 874
S_CYBRAKDEMONNAPALMFLAME_DIE = 875
S_CYBRAKDEMONVILEEXPLOSION1 = 876
S_CYBRAKDEMONVILEEXPLOSION2 = 877
S_CYBRAKDEMONVILEEXPLOSION3 = 878
S_METALSONIC_RACE = 879
S_METALSONIC_FLOAT = 880
S_METALSONIC_VECTOR = 881
S_METALSONIC_STUN = 882
S_METALSONIC_RAISE = 883
S_METALSONIC_GATHER = 884
S_METALSONIC_DASH = 885
S_METALSONIC_BOUNCE = 886
S_METALSONIC_BADBOUNCE = 887
S_METALSONIC_SHOOT = 888
S_METALSONIC_PAIN = 889
S_METALSONIC_DEATH1 = 890
S_METALSONIC_DEATH2 = 891
S_METALSONIC_DEATH3 = 892
S_METALSONIC_DEATH4 = 893
S_METALSONIC_FLEE1 = 894
S_METALSONIC_FLEE2 = 895
S_MSSHIELD_F1 = 896
S_MSSHIELD_F2 = 897
S_RING = 898
S_BLUESPHERE = 899
S_BLUESPHEREBONUS = 900
S_BLUESPHERESPARK = 901
S_BOMBSPHERE1 = 902
S_BOMBSPHERE2 = 903
S_BOMBSPHERE3 = 904
S_BOMBSPHERE4 = 905
S_NIGHTSCHIP = 906
S_NIGHTSCHIPBONUS = 907
S_NIGHTSSTAR = 908
S_NIGHTSSTARXMAS = 909
S_GRAVWELLGREEN = 910
S_GRAVWELLRED = 911
S_TEAMRING = 912
S_TOKEN = 913
S_REDFLAG = 914
S_BLUEFLAG = 915
S_EMBLEM1 = 916
S_EMBLEM2 = 917
S_EMBLEM3 = 918
S_EMBLEM4 = 919
S_EMBLEM5 = 920
S_EMBLEM6 = 921
S_EMBLEM7 = 922
S_EMBLEM8 = 923
S_EMBLEM9 = 924
S_EMBLEM10 = 925
S_EMBLEM11 = 926
S_EMBLEM12 = 927
S_EMBLEM13 = 928
S_EMBLEM14 = 929
S_EMBLEM15 = 930
S_EMBLEM16 = 931
S_EMBLEM17 = 932
S_EMBLEM18 = 933
S_EMBLEM19 = 934
S_EMBLEM20 = 935
S_EMBLEM21 = 936
S_EMBLEM22 = 937
S_EMBLEM23 = 938
S_EMBLEM24 = 939
S_EMBLEM25 = 940
S_EMBLEM26 = 941
S_CEMG1 = 942
S_CEMG2 = 943
S_CEMG3 = 944
S_CEMG4 = 945
S_CEMG5 = 946
S_CEMG6 = 947
S_CEMG7 = 948
S_SHRD1 = 949
S_SHRD2 = 950
S_SHRD3 = 951
S_BUBBLES1 = 952
S_BUBBLES2 = 953
S_BUBBLES3 = 954
S_BUBBLES4 = 955
S_SIGN = 956
S_SIGNSPIN1 = 957
S_SIGNSPIN2 = 958
S_SIGNSPIN3 = 959
S_SIGNSPIN4 = 960
S_SIGNSPIN5 = 961
S_SIGNSPIN6 = 962
S_SIGNPLAYER = 963
S_SIGNSLOW = 964
S_SIGNSTOP = 965
S_SIGNBOARD = 966
S_EGGMANSIGN = 967
S_CLEARSIGN = 968
S_SPIKEBALL1 = 969
S_SPIKEBALL2 = 970
S_SPIKEBALL3 = 971
S_SPIKEBALL4 = 972
S_SPIKEBALL5 = 973
S_SPIKEBALL6 = 974
S_SPIKEBALL7 = 975
S_SPIKEBALL8 = 976
S_SPINFIRE1 = 977
S_SPINFIRE2 = 978
S_SPINFIRE3 = 979
S_SPINFIRE4 = 980
S_SPINFIRE5 = 981
S_SPINFIRE6 = 982
S_TEAM_SPINFIRE1 = 983
S_TEAM_SPINFIRE2 = 984
S_TEAM_SPINFIRE3 = 985
S_TEAM_SPINFIRE4 = 986
S_TEAM_SPINFIRE5 = 987
S_TEAM_SPINFIRE6 = 988
S_SPIKE1 = 989
S_SPIKE2 = 990
S_SPIKE3 = 991
S_SPIKE4 = 992
S_SPIKE5 = 993
S_SPIKE6 = 994
S_SPIKED1 = 995
S_SPIKED2 = 996
S_WALLSPIKE1 = 997
S_WALLSPIKE2 = 998
S_WALLSPIKE3 = 999
S_WALLSPIKE4 = 1000
S_WALLSPIKE5 = 1001
S_WALLSPIKE6 = 1002
S_WALLSPIKEBASE = 1003
S_WALLSPIKED1 = 1004
S_WALLSPIKED2 = 1005
S_STARPOST_IDLE = 1006
S_STARPOST_FLASH = 1007
S_STARPOST_STARTSPIN = 1008
S_STARPOST_SPIN = 1009
S_STARPOST_ENDSPIN = 1010
S_BIGMINE_IDLE = 1011
S_BIGMINE_ALERT1 = 1012
S_BIGMINE_ALERT2 = 1013
S_BIGMINE_ALERT3 = 1014
S_BIGMINE_SET1 = 1015
S_BIGMINE_SET2 = 1016
S_BIGMINE_SET3 = 1017
S_BIGMINE_BLAST1 = 1018
S_BIGMINE_BLAST2 = 1019
S_BIGMINE_BLAST3 = 1020
S_BIGMINE_BLAST4 = 1021
S_BIGMINE_BLAST5 = 1022
S_CANNONLAUNCHER1 = 1023
S_CANNONLAUNCHER2 = 1024
S_CANNONLAUNCHER3 = 1025
S_BOXSPARKLE1 = 1026
S_BOXSPARKLE2 = 1027
S_BOXSPARKLE3 = 1028
S_BOXSPARKLE4 = 1029
S_BOX_FLICKER = 1030
S_BOX_POP1 = 1031
S_BOX_POP2 = 1032
S_GOLDBOX_FLICKER = 1033
S_GOLDBOX_OFF1 = 1034
S_GOLDBOX_OFF2 = 1035
S_GOLDBOX_OFF3 = 1036
S_GOLDBOX_OFF4 = 1037
S_GOLDBOX_OFF5 = 1038
S_GOLDBOX_OFF6 = 1039
S_GOLDBOX_OFF7 = 1040
S_MYSTERY_BOX = 1041
S_RING_BOX = 1042
S_PITY_BOX = 1043
S_ATTRACT_BOX = 1044
S_FORCE_BOX = 1045
S_ARMAGEDDON_BOX = 1046
S_WHIRLWIND_BOX = 1047
S_ELEMENTAL_BOX = 1048
S_SNEAKERS_BOX = 1049
S_INVULN_BOX = 1050
S_1UP_BOX = 1051
S_EGGMAN_BOX = 1052
S_MIXUP_BOX = 1053
S_GRAVITY_BOX = 1054
S_RECYCLER_BOX = 1055
S_SCORE1K_BOX = 1056
S_SCORE10K_BOX = 1057
S_FLAMEAURA_BOX = 1058
S_BUBBLEWRAP_BOX = 1059
S_THUNDERCOIN_BOX = 1060
S_PITY_GOLDBOX = 1061
S_ATTRACT_GOLDBOX = 1062
S_FORCE_GOLDBOX = 1063
S_ARMAGEDDON_GOLDBOX = 1064
S_WHIRLWIND_GOLDBOX = 1065
S_ELEMENTAL_GOLDBOX = 1066
S_SNEAKERS_GOLDBOX = 1067
S_INVULN_GOLDBOX = 1068
S_EGGMAN_GOLDBOX = 1069
S_GRAVITY_GOLDBOX = 1070
S_FLAMEAURA_GOLDBOX = 1071
S_BUBBLEWRAP_GOLDBOX = 1072
S_THUNDERCOIN_GOLDBOX = 1073
S_RING_REDBOX1 = 1074
S_RING_REDBOX2 = 1075
S_REDBOX_POP1 = 1076
S_REDBOX_POP2 = 1077
S_RING_BLUEBOX1 = 1078
S_RING_BLUEBOX2 = 1079
S_BLUEBOX_POP1 = 1080
S_BLUEBOX_POP2 = 1081
S_RING_ICON1 = 1082
S_RING_ICON2 = 1083
S_PITY_ICON1 = 1084
S_PITY_ICON2 = 1085
S_ATTRACT_ICON1 = 1086
S_ATTRACT_ICON2 = 1087
S_FORCE_ICON1 = 1088
S_FORCE_ICON2 = 1089
S_ARMAGEDDON_ICON1 = 1090
S_ARMAGEDDON_ICON2 = 1091
S_WHIRLWIND_ICON1 = 1092
S_WHIRLWIND_ICON2 = 1093
S_ELEMENTAL_ICON1 = 1094
S_ELEMENTAL_ICON2 = 1095
S_SNEAKERS_ICON1 = 1096
S_SNEAKERS_ICON2 = 1097
S_INVULN_ICON1 = 1098
S_INVULN_ICON2 = 1099
S_1UP_ICON1 = 1100
S_1UP_ICON2 = 1101
S_EGGMAN_ICON1 = 1102
S_EGGMAN_ICON2 = 1103
S_MIXUP_ICON1 = 1104
S_MIXUP_ICON2 = 1105
S_GRAVITY_ICON1 = 1106
S_GRAVITY_ICON2 = 1107
S_RECYCLER_ICON1 = 1108
S_RECYCLER_ICON2 = 1109
S_SCORE1K_ICON1 = 1110
S_SCORE1K_ICON2 = 1111
S_SCORE10K_ICON1 = 1112
S_SCORE10K_ICON2 = 1113
S_FLAMEAURA_ICON1 = 1114
S_FLAMEAURA_ICON2 = 1115
S_BUBBLEWRAP_ICON1 = 1116
S_BUBBLEWRAP_ICON2 = 1117
S_THUNDERCOIN_ICON1 = 1118
S_THUNDERCOIN_ICON2 = 1119
S_ROCKET = 1120
S_LASER = 1121
S_LASER2 = 1122
S_LASERFLASH = 1123
S_LASERFLAME1 = 1124
S_LASERFLAME2 = 1125
S_LASERFLAME3 = 1126
S_LASERFLAME4 = 1127
S_LASERFLAME5 = 1128
S_TORPEDO = 1129
S_ENERGYBALL1 = 1130
S_ENERGYBALL2 = 1131
S_MINE1 = 1132
S_MINE_BOOM1 = 1133
S_MINE_BOOM2 = 1134
S_MINE_BOOM3 = 1135
S_MINE_BOOM4 = 1136
S_JETBULLET1 = 1137
S_JETBULLET2 = 1138
S_TURRETLASER = 1139
S_TURRETLASEREXPLODE1 = 1140
S_TURRETLASEREXPLODE2 = 1141
S_CANNONBALL1 = 1142
S_ARROW = 1143
S_ARROWBONK = 1144
S_DEMONFIRE = 1145
S_LETTER = 1146
S_TUTORIALLEAF1 = 1147
S_TUTORIALLEAF2 = 1148
S_TUTORIALLEAF3 = 1149
S_TUTORIALLEAF4 = 1150
S_TUTORIALLEAF5 = 1151
S_TUTORIALLEAF6 = 1152
S_TUTORIALLEAF7 = 1153
S_TUTORIALLEAF8 = 1154
S_TUTORIALLEAF9 = 1155
S_TUTORIALLEAF10 = 1156
S_TUTORIALLEAF11 = 1157
S_TUTORIALLEAF12 = 1158
S_TUTORIALLEAF13 = 1159
S_TUTORIALLEAF14 = 1160
S_TUTORIALLEAF15 = 1161
S_TUTORIALLEAF16 = 1162
S_TUTORIALFLOWER1 = 1163
S_TUTORIALFLOWER2 = 1164
S_TUTORIALFLOWER3 = 1165
S_TUTORIALFLOWER4 = 1166
S_TUTORIALFLOWER5 = 1167
S_TUTORIALFLOWER6 = 1168
S_TUTORIALFLOWER7 = 1169
S_TUTORIALFLOWER8 = 1170
S_TUTORIALFLOWER9 = 1171
S_TUTORIALFLOWER10 = 1172
S_TUTORIALFLOWER11 = 1173
S_TUTORIALFLOWER12 = 1174
S_TUTORIALFLOWER13 = 1175
S_TUTORIALFLOWER14 = 1176
S_TUTORIALFLOWER15 = 1177
S_TUTORIALFLOWER16 = 1178
S_TUTORIALFLOWERF1 = 1179
S_TUTORIALFLOWERF2 = 1180
S_TUTORIALFLOWERF3 = 1181
S_TUTORIALFLOWERF4 = 1182
S_TUTORIALFLOWERF5 = 1183
S_TUTORIALFLOWERF6 = 1184
S_TUTORIALFLOWERF7 = 1185
S_TUTORIALFLOWERF8 = 1186
S_TUTORIALFLOWERF9 = 1187
S_TUTORIALFLOWERF10 = 1188
S_TUTORIALFLOWERF11 = 1189
S_TUTORIALFLOWERF12 = 1190
S_TUTORIALFLOWERF13 = 1191
S_TUTORIALFLOWERF14 = 1192
S_TUTORIALFLOWERF15 = 1193
S_TUTORIALFLOWERF16 = 1194
S_GFZFLOWERA = 1195
S_GFZFLOWERB = 1196
S_GFZFLOWERC = 1197
S_BLUEBERRYBUSH = 1198
S_BERRYBUSH = 1199
S_BUSH = 1200
S_GFZTREE = 1201
S_GFZBERRYTREE = 1202
S_GFZCHERRYTREE = 1203
S_CHECKERTREE = 1204
S_CHECKERSUNSETTREE = 1205
S_FHZTREE = 1206
S_FHZPINKTREE = 1207
S_POLYGONTREE = 1208
S_BUSHTREE = 1209
S_BUSHREDTREE = 1210
S_SPRINGTREE = 1211
S_THZFLOWERA = 1212
S_THZFLOWERB = 1213
S_THZFLOWERC = 1214
S_THZTREE = 1215
S_THZTREEBRANCH1 = 1216
S_THZTREEBRANCH2 = 1217
S_THZTREEBRANCH3 = 1218
S_THZTREEBRANCH4 = 1219
S_THZTREEBRANCH5 = 1220
S_THZTREEBRANCH6 = 1221
S_THZTREEBRANCH7 = 1222
S_THZTREEBRANCH8 = 1223
S_THZTREEBRANCH9 = 1224
S_THZTREEBRANCH10 = 1225
S_THZTREEBRANCH11 = 1226
S_THZTREEBRANCH12 = 1227
S_THZTREEBRANCH13 = 1228
S_ALARM1 = 1229
S_GARGOYLE = 1230
S_BIGGARGOYLE = 1231
S_SEAWEED1 = 1232
S_SEAWEED2 = 1233
S_SEAWEED3 = 1234
S_SEAWEED4 = 1235
S_SEAWEED5 = 1236
S_SEAWEED6 = 1237
S_DRIPA1 = 1238
S_DRIPA2 = 1239
S_DRIPA3 = 1240
S_DRIPA4 = 1241
S_DRIPB1 = 1242
S_DRIPC1 = 1243
S_DRIPC2 = 1244
S_CORAL1 = 1245
S_CORAL2 = 1246
S_CORAL3 = 1247
S_CORAL4 = 1248
S_CORAL5 = 1249
S_BLUECRYSTAL1 = 1250
S_KELP = 1251
S_ANIMALGAETOP1 = 1252
S_ANIMALGAETOP2 = 1253
S_ANIMALGAESEG = 1254
S_DSZSTALAGMITE = 1255
S_DSZ2STALAGMITE = 1256
S_LIGHTBEAM1 = 1257
S_LIGHTBEAM2 = 1258
S_LIGHTBEAM3 = 1259
S_LIGHTBEAM4 = 1260
S_LIGHTBEAM5 = 1261
S_LIGHTBEAM6 = 1262
S_LIGHTBEAM7 = 1263
S_LIGHTBEAM8 = 1264
S_LIGHTBEAM9 = 1265
S_LIGHTBEAM10 = 1266
S_LIGHTBEAM11 = 1267
S_LIGHTBEAM12 = 1268
S_CEZCHAIN = 1269
S_FLAME = 1270
S_FLAMEPARTICLE = 1271
S_FLAMEREST = 1272
S_EGGSTATUE1 = 1273
S_SLING1 = 1274
S_SLING2 = 1275
S_SMALLMACECHAIN = 1276
S_BIGMACECHAIN = 1277
S_SMALLMACE = 1278
S_BIGMACE = 1279
S_SMALLGRABCHAIN = 1280
S_BIGGRABCHAIN = 1281
S_YELLOWSPRINGBALL = 1282
S_YELLOWSPRINGBALL2 = 1283
S_YELLOWSPRINGBALL3 = 1284
S_YELLOWSPRINGBALL4 = 1285
S_YELLOWSPRINGBALL5 = 1286
S_REDSPRINGBALL = 1287
S_REDSPRINGBALL2 = 1288
S_REDSPRINGBALL3 = 1289
S_REDSPRINGBALL4 = 1290
S_REDSPRINGBALL5 = 1291
S_SMALLFIREBAR1 = 1292
S_SMALLFIREBAR2 = 1293
S_SMALLFIREBAR3 = 1294
S_SMALLFIREBAR4 = 1295
S_SMALLFIREBAR5 = 1296
S_SMALLFIREBAR6 = 1297
S_SMALLFIREBAR7 = 1298
S_SMALLFIREBAR8 = 1299
S_SMALLFIREBAR9 = 1300
S_SMALLFIREBAR10 = 1301
S_SMALLFIREBAR11 = 1302
S_SMALLFIREBAR12 = 1303
S_SMALLFIREBAR13 = 1304
S_SMALLFIREBAR14 = 1305
S_SMALLFIREBAR15 = 1306
S_SMALLFIREBAR16 = 1307
S_BIGFIREBAR1 = 1308
S_BIGFIREBAR2 = 1309
S_BIGFIREBAR3 = 1310
S_BIGFIREBAR4 = 1311
S_BIGFIREBAR5 = 1312
S_BIGFIREBAR6 = 1313
S_BIGFIREBAR7 = 1314
S_BIGFIREBAR8 = 1315
S_BIGFIREBAR9 = 1316
S_BIGFIREBAR10 = 1317
S_BIGFIREBAR11 = 1318
S_BIGFIREBAR12 = 1319
S_BIGFIREBAR13 = 1320
S_BIGFIREBAR14 = 1321
S_BIGFIREBAR15 = 1322
S_BIGFIREBAR16 = 1323
S_CEZFLOWER = 1324
S_CEZPOLE = 1325
S_CEZBANNER1 = 1326
S_CEZBANNER2 = 1327
S_PINETREE = 1328
S_CEZBUSH1 = 1329
S_CEZBUSH2 = 1330
S_CANDLE = 1331
S_CANDLEPRICKET = 1332
S_FLAMEHOLDER = 1333
S_FIRETORCH = 1334
S_WAVINGFLAG = 1335
S_WAVINGFLAGSEG1 = 1336
S_WAVINGFLAGSEG2 = 1337
S_CRAWLASTATUE = 1338
S_FACESTABBERSTATUE = 1339
S_SUSPICIOUSFACESTABBERSTATUE_WAIT = 1340
S_SUSPICIOUSFACESTABBERSTATUE_BURST1 = 1341
S_SUSPICIOUSFACESTABBERSTATUE_BURST2 = 1342
S_BRAMBLES = 1343
S_BIGTUMBLEWEED = 1344
S_BIGTUMBLEWEED_ROLL1 = 1345
S_BIGTUMBLEWEED_ROLL2 = 1346
S_BIGTUMBLEWEED_ROLL3 = 1347
S_BIGTUMBLEWEED_ROLL4 = 1348
S_BIGTUMBLEWEED_ROLL5 = 1349
S_BIGTUMBLEWEED_ROLL6 = 1350
S_BIGTUMBLEWEED_ROLL7 = 1351
S_BIGTUMBLEWEED_ROLL8 = 1352
S_LITTLETUMBLEWEED = 1353
S_LITTLETUMBLEWEED_ROLL1 = 1354
S_LITTLETUMBLEWEED_ROLL2 = 1355
S_LITTLETUMBLEWEED_ROLL3 = 1356
S_LITTLETUMBLEWEED_ROLL4 = 1357
S_LITTLETUMBLEWEED_ROLL5 = 1358
S_LITTLETUMBLEWEED_ROLL6 = 1359
S_LITTLETUMBLEWEED_ROLL7 = 1360
S_LITTLETUMBLEWEED_ROLL8 = 1361
S_CACTI1 = 1362
S_CACTI2 = 1363
S_CACTI3 = 1364
S_CACTI4 = 1365
S_CACTI5 = 1366
S_CACTI6 = 1367
S_CACTI7 = 1368
S_CACTI8 = 1369
S_CACTI9 = 1370
S_CACTI10 = 1371
S_CACTI11 = 1372
S_CACTITINYSEG = 1373
S_CACTISMALLSEG = 1374
S_ARIDSIGN_CAUTION = 1375
S_ARIDSIGN_CACTI = 1376
S_ARIDSIGN_SHARPTURN = 1377
S_OILLAMP = 1378
S_OILLAMPFLARE = 1379
S_TNTBARREL_STND1 = 1380
S_TNTBARREL_EXPL1 = 1381
S_TNTBARREL_EXPL2 = 1382
S_TNTBARREL_EXPL3 = 1383
S_TNTBARREL_EXPL4 = 1384
S_TNTBARREL_EXPL5 = 1385
S_TNTBARREL_EXPL6 = 1386
S_TNTBARREL_EXPL7 = 1387
S_TNTBARREL_FLYING = 1388
S_PROXIMITY_TNT = 1389
S_PROXIMITY_TNT_TRIGGER1 = 1390
S_PROXIMITY_TNT_TRIGGER2 = 1391
S_PROXIMITY_TNT_TRIGGER3 = 1392
S_PROXIMITY_TNT_TRIGGER4 = 1393
S_PROXIMITY_TNT_TRIGGER5 = 1394
S_PROXIMITY_TNT_TRIGGER6 = 1395
S_PROXIMITY_TNT_TRIGGER7 = 1396
S_PROXIMITY_TNT_TRIGGER8 = 1397
S_PROXIMITY_TNT_TRIGGER9 = 1398
S_PROXIMITY_TNT_TRIGGER10 = 1399
S_PROXIMITY_TNT_TRIGGER11 = 1400
S_PROXIMITY_TNT_TRIGGER12 = 1401
S_PROXIMITY_TNT_TRIGGER13 = 1402
S_PROXIMITY_TNT_TRIGGER14 = 1403
S_PROXIMITY_TNT_TRIGGER15 = 1404
S_PROXIMITY_TNT_TRIGGER16 = 1405
S_PROXIMITY_TNT_TRIGGER17 = 1406
S_PROXIMITY_TNT_TRIGGER18 = 1407
S_PROXIMITY_TNT_TRIGGER19 = 1408
S_PROXIMITY_TNT_TRIGGER20 = 1409
S_PROXIMITY_TNT_TRIGGER21 = 1410
S_PROXIMITY_TNT_TRIGGER22 = 1411
S_PROXIMITY_TNT_TRIGGER23 = 1412
S_DUSTDEVIL = 1413
S_DUSTLAYER1 = 1414
S_DUSTLAYER2 = 1415
S_DUSTLAYER3 = 1416
S_DUSTLAYER4 = 1417
S_DUSTLAYER5 = 1418
S_ARIDDUST1 = 1419
S_ARIDDUST2 = 1420
S_ARIDDUST3 = 1421
S_MINECART_IDLE = 1422
S_MINECART_DTH1 = 1423
S_MINECARTEND = 1424
S_MINECARTSEG_FRONT = 1425
S_MINECARTSEG_BACK = 1426
S_MINECARTSEG_LEFT = 1427
S_MINECARTSEG_RIGHT = 1428
S_MINECARTSIDEMARK1 = 1429
S_MINECARTSIDEMARK2 = 1430
S_MINECARTSPARK = 1431
S_SALOONDOOR = 1432
S_SALOONDOORCENTER = 1433
S_TRAINCAMEOSPAWNER_1 = 1434
S_TRAINCAMEOSPAWNER_2 = 1435
S_TRAINCAMEOSPAWNER_3 = 1436
S_TRAINCAMEOSPAWNER_4 = 1437
S_TRAINCAMEOSPAWNER_5 = 1438
S_TRAINPUFFMAKER = 1439
S_TRAINDUST = 1440
S_TRAINSTEAM = 1441
S_FLAMEJETSTND = 1442
S_FLAMEJETSTART = 1443
S_FLAMEJETSTOP = 1444
S_FLAMEJETFLAME1 = 1445
S_FLAMEJETFLAME2 = 1446
S_FLAMEJETFLAME3 = 1447
S_FLAMEJETFLAME4 = 1448
S_FLAMEJETFLAME5 = 1449
S_FLAMEJETFLAME6 = 1450
S_FLAMEJETFLAME7 = 1451
S_FLAMEJETFLAME8 = 1452
S_FLAMEJETFLAME9 = 1453
S_FJSPINAXISA1 = 1454
S_FJSPINAXISA2 = 1455
S_FJSPINAXISB1 = 1456
S_FJSPINAXISB2 = 1457
S_FLAMEJETFLAMEB1 = 1458
S_FLAMEJETFLAMEB2 = 1459
S_FLAMEJETFLAMEB3 = 1460
S_LAVAFALL_DORMANT = 1461
S_LAVAFALL_TELL = 1462
S_LAVAFALL_SHOOT = 1463
S_LAVAFALL_LAVA1 = 1464
S_LAVAFALL_LAVA2 = 1465
S_LAVAFALL_LAVA3 = 1466
S_LAVAFALLROCK = 1467
S_ROLLOUTSPAWN = 1468
S_ROLLOUTROCK = 1469
S_BIGFERNLEAF = 1470
S_BIGFERN1 = 1471
S_BIGFERN2 = 1472
S_JUNGLEPALM = 1473
S_TORCHFLOWER = 1474
S_WALLVINE_LONG = 1475
S_WALLVINE_SHORT = 1476
S_GLAREGOYLE = 1477
S_GLAREGOYLE_CHARGE = 1478
S_GLAREGOYLE_BLINK = 1479
S_GLAREGOYLE_HOLD = 1480
S_GLAREGOYLE_FIRE = 1481
S_GLAREGOYLE_LOOP = 1482
S_GLAREGOYLE_COOLDOWN = 1483
S_GLAREGOYLEUP = 1484
S_GLAREGOYLEUP_CHARGE = 1485
S_GLAREGOYLEUP_BLINK = 1486
S_GLAREGOYLEUP_HOLD = 1487
S_GLAREGOYLEUP_FIRE = 1488
S_GLAREGOYLEUP_LOOP = 1489
S_GLAREGOYLEUP_COOLDOWN = 1490
S_GLAREGOYLEDOWN = 1491
S_GLAREGOYLEDOWN_CHARGE = 1492
S_GLAREGOYLEDOWN_BLINK = 1493
S_GLAREGOYLEDOWN_HOLD = 1494
S_GLAREGOYLEDOWN_FIRE = 1495
S_GLAREGOYLEDOWN_LOOP = 1496
S_GLAREGOYLEDOWN_COOLDOWN = 1497
S_GLAREGOYLELONG = 1498
S_GLAREGOYLELONG_CHARGE = 1499
S_GLAREGOYLELONG_BLINK = 1500
S_GLAREGOYLELONG_HOLD = 1501
S_GLAREGOYLELONG_FIRE = 1502
S_GLAREGOYLELONG_LOOP = 1503
S_GLAREGOYLELONG_COOLDOWN = 1504
S_TARGET_IDLE = 1505
S_TARGET_HIT1 = 1506
S_TARGET_HIT2 = 1507
S_TARGET_RESPAWN = 1508
S_TARGET_ALLDONE = 1509
S_GREENFLAME = 1510
S_BLUEGARGOYLE = 1511
S_STG0 = 1512
S_STG1 = 1513
S_STG2 = 1514
S_STG3 = 1515
S_STG4 = 1516
S_STG5 = 1517
S_STG6 = 1518
S_STG7 = 1519
S_STG8 = 1520
S_STG9 = 1521
S_XMASPOLE = 1522
S_CANDYCANE = 1523
S_SNOWMAN = 1524
S_SNOWMANHAT = 1525
S_LAMPPOST1 = 1526
S_LAMPPOST2 = 1527
S_HANGSTAR = 1528
S_MISTLETOE = 1529
S_XMASBLUEBERRYBUSH = 1530
S_XMASBERRYBUSH = 1531
S_XMASBUSH = 1532
S_FHZICE1 = 1533
S_FHZICE2 = 1534
S_ROSY_IDLE1 = 1535
S_ROSY_IDLE2 = 1536
S_ROSY_IDLE3 = 1537
S_ROSY_IDLE4 = 1538
S_ROSY_JUMP = 1539
S_ROSY_WALK = 1540
S_ROSY_HUG = 1541
S_ROSY_PAIN = 1542
S_ROSY_STND = 1543
S_ROSY_UNHAPPY = 1544
S_JACKO1 = 1545
S_JACKO1OVERLAY_1 = 1546
S_JACKO1OVERLAY_2 = 1547
S_JACKO1OVERLAY_3 = 1548
S_JACKO1OVERLAY_4 = 1549
S_JACKO2 = 1550
S_JACKO2OVERLAY_1 = 1551
S_JACKO2OVERLAY_2 = 1552
S_JACKO2OVERLAY_3 = 1553
S_JACKO2OVERLAY_4 = 1554
S_JACKO3 = 1555
S_JACKO3OVERLAY_1 = 1556
S_JACKO3OVERLAY_2 = 1557
S_JACKO3OVERLAY_3 = 1558
S_JACKO3OVERLAY_4 = 1559
S_HHZTREE_TOP = 1560
S_HHZTREE_TRUNK = 1561
S_HHZTREE_LEAF = 1562
S_HHZSHROOM_1 = 1563
S_HHZSHROOM_2 = 1564
S_HHZSHROOM_3 = 1565
S_HHZSHROOM_4 = 1566
S_HHZSHROOM_5 = 1567
S_HHZSHROOM_6 = 1568
S_HHZSHROOM_7 = 1569
S_HHZSHROOM_8 = 1570
S_HHZSHROOM_9 = 1571
S_HHZSHROOM_10 = 1572
S_HHZSHROOM_11 = 1573
S_HHZSHROOM_12 = 1574
S_HHZSHROOM_13 = 1575
S_HHZSHROOM_14 = 1576
S_HHZSHROOM_15 = 1577
S_HHZSHROOM_16 = 1578
S_HHZGRASS = 1579
S_HHZTENT1 = 1580
S_HHZTENT2 = 1581
S_HHZSTALAGMITE_TALL = 1582
S_HHZSTALAGMITE_SHORT = 1583
S_BSZTALLFLOWER_RED = 1584
S_BSZTALLFLOWER_PURPLE = 1585
S_BSZTALLFLOWER_BLUE = 1586
S_BSZTALLFLOWER_CYAN = 1587
S_BSZTALLFLOWER_YELLOW = 1588
S_BSZTALLFLOWER_ORANGE = 1589
S_BSZFLOWER_RED = 1590
S_BSZFLOWER_PURPLE = 1591
S_BSZFLOWER_BLUE = 1592
S_BSZFLOWER_CYAN = 1593
S_BSZFLOWER_YELLOW = 1594
S_BSZFLOWER_ORANGE = 1595
S_BSZSHORTFLOWER_RED = 1596
S_BSZSHORTFLOWER_PURPLE = 1597
S_BSZSHORTFLOWER_BLUE = 1598
S_BSZSHORTFLOWER_CYAN = 1599
S_BSZSHORTFLOWER_YELLOW = 1600
S_BSZSHORTFLOWER_ORANGE = 1601
S_BSZTULIP_RED = 1602
S_BSZTULIP_PURPLE = 1603
S_BSZTULIP_BLUE = 1604
S_BSZTULIP_CYAN = 1605
S_BSZTULIP_YELLOW = 1606
S_BSZTULIP_ORANGE = 1607
S_BSZCLUSTER_RED = 1608
S_BSZCLUSTER_PURPLE = 1609
S_BSZCLUSTER_BLUE = 1610
S_BSZCLUSTER_CYAN = 1611
S_BSZCLUSTER_YELLOW = 1612
S_BSZCLUSTER_ORANGE = 1613
S_BSZBUSH_RED = 1614
S_BSZBUSH_PURPLE = 1615
S_BSZBUSH_BLUE = 1616
S_BSZBUSH_CYAN = 1617
S_BSZBUSH_YELLOW = 1618
S_BSZBUSH_ORANGE = 1619
S_BSZVINE_RED = 1620
S_BSZVINE_PURPLE = 1621
S_BSZVINE_BLUE = 1622
S_BSZVINE_CYAN = 1623
S_BSZVINE_YELLOW = 1624
S_BSZVINE_ORANGE = 1625
S_BSZSHRUB = 1626
S_BSZCLOVER = 1627
S_BIG_PALMTREE_TRUNK = 1628
S_BIG_PALMTREE_TOP = 1629
S_PALMTREE_TRUNK = 1630
S_PALMTREE_TOP = 1631
S_DBALL1 = 1632
S_DBALL2 = 1633
S_DBALL3 = 1634
S_DBALL4 = 1635
S_DBALL5 = 1636
S_DBALL6 = 1637
S_EGGSTATUE2 = 1638
S_ARMA1 = 1639
S_ARMA2 = 1640
S_ARMA3 = 1641
S_ARMA4 = 1642
S_ARMA5 = 1643
S_ARMA6 = 1644
S_ARMA7 = 1645
S_ARMA8 = 1646
S_ARMA9 = 1647
S_ARMA10 = 1648
S_ARMA11 = 1649
S_ARMA12 = 1650
S_ARMA13 = 1651
S_ARMA14 = 1652
S_ARMA15 = 1653
S_ARMA16 = 1654
S_ARMF1 = 1655
S_ARMF2 = 1656
S_ARMF3 = 1657
S_ARMF4 = 1658
S_ARMF5 = 1659
S_ARMF6 = 1660
S_ARMF7 = 1661
S_ARMF8 = 1662
S_ARMF9 = 1663
S_ARMF10 = 1664
S_ARMF11 = 1665
S_ARMF12 = 1666
S_ARMF13 = 1667
S_ARMF14 = 1668
S_ARMF15 = 1669
S_ARMF16 = 1670
S_ARMF17 = 1671
S_ARMF18 = 1672
S_ARMF19 = 1673
S_ARMF20 = 1674
S_ARMF21 = 1675
S_ARMF22 = 1676
S_ARMF23 = 1677
S_ARMF24 = 1678
S_ARMF25 = 1679
S_ARMF26 = 1680
S_ARMF27 = 1681
S_ARMF28 = 1682
S_ARMF29 = 1683
S_ARMF30 = 1684
S_ARMF31 = 1685
S_ARMF32 = 1686
S_ARMB1 = 1687
S_ARMB2 = 1688
S_ARMB3 = 1689
S_ARMB4 = 1690
S_ARMB5 = 1691
S_ARMB6 = 1692
S_ARMB7 = 1693
S_ARMB8 = 1694
S_ARMB9 = 1695
S_ARMB10 = 1696
S_ARMB11 = 1697
S_ARMB12 = 1698
S_ARMB13 = 1699
S_ARMB14 = 1700
S_ARMB15 = 1701
S_ARMB16 = 1702
S_ARMB17 = 1703
S_ARMB18 = 1704
S_ARMB19 = 1705
S_ARMB20 = 1706
S_ARMB21 = 1707
S_ARMB22 = 1708
S_ARMB23 = 1709
S_ARMB24 = 1710
S_ARMB25 = 1711
S_ARMB26 = 1712
S_ARMB27 = 1713
S_ARMB28 = 1714
S_ARMB29 = 1715
S_ARMB30 = 1716
S_ARMB31 = 1717
S_ARMB32 = 1718
S_WIND1 = 1719
S_WIND2 = 1720
S_WIND3 = 1721
S_WIND4 = 1722
S_WIND5 = 1723
S_WIND6 = 1724
S_WIND7 = 1725
S_WIND8 = 1726
S_MAGN1 = 1727
S_MAGN2 = 1728
S_MAGN3 = 1729
S_MAGN4 = 1730
S_MAGN5 = 1731
S_MAGN6 = 1732
S_MAGN7 = 1733
S_MAGN8 = 1734
S_MAGN9 = 1735
S_MAGN10 = 1736
S_MAGN11 = 1737
S_MAGN12 = 1738
S_MAGN13 = 1739
S_FORC1 = 1740
S_FORC2 = 1741
S_FORC3 = 1742
S_FORC4 = 1743
S_FORC5 = 1744
S_FORC6 = 1745
S_FORC7 = 1746
S_FORC8 = 1747
S_FORC9 = 1748
S_FORC10 = 1749
S_FORC11 = 1750
S_FORC12 = 1751
S_FORC13 = 1752
S_FORC14 = 1753
S_FORC15 = 1754
S_FORC16 = 1755
S_FORC17 = 1756
S_FORC18 = 1757
S_FORC19 = 1758
S_FORC20 = 1759
S_FORC21 = 1760
S_ELEM1 = 1761
S_ELEM2 = 1762
S_ELEM3 = 1763
S_ELEM4 = 1764
S_ELEM5 = 1765
S_ELEM6 = 1766
S_ELEM7 = 1767
S_ELEM8 = 1768
S_ELEM9 = 1769
S_ELEM10 = 1770
S_ELEM11 = 1771
S_ELEM12 = 1772
S_ELEM13 = 1773
S_ELEM14 = 1774
S_ELEMF1 = 1775
S_ELEMF2 = 1776
S_ELEMF3 = 1777
S_ELEMF4 = 1778
S_ELEMF5 = 1779
S_ELEMF6 = 1780
S_ELEMF7 = 1781
S_ELEMF8 = 1782
S_ELEMF9 = 1783
S_ELEMF10 = 1784
S_PITY1 = 1785
S_PITY2 = 1786
S_PITY3 = 1787
S_PITY4 = 1788
S_PITY5 = 1789
S_PITY6 = 1790
S_PITY7 = 1791
S_PITY8 = 1792
S_PITY9 = 1793
S_PITY10 = 1794
S_PITY11 = 1795
S_PITY12 = 1796
S_FIRS1 = 1797
S_FIRS2 = 1798
S_FIRS3 = 1799
S_FIRS4 = 1800
S_FIRS5 = 1801
S_FIRS6 = 1802
S_FIRS7 = 1803
S_FIRS8 = 1804
S_FIRS9 = 1805
S_FIRS10 = 1806
S_FIRS11 = 1807
S_FIRSB1 = 1808
S_FIRSB2 = 1809
S_FIRSB3 = 1810
S_FIRSB4 = 1811
S_FIRSB5 = 1812
S_FIRSB6 = 1813
S_FIRSB7 = 1814
S_FIRSB8 = 1815
S_FIRSB9 = 1816
S_FIRSB10 = 1817
S_BUBS1 = 1818
S_BUBS2 = 1819
S_BUBS3 = 1820
S_BUBS4 = 1821
S_BUBS5 = 1822
S_BUBS6 = 1823
S_BUBS7 = 1824
S_BUBS8 = 1825
S_BUBS9 = 1826
S_BUBS10 = 1827
S_BUBS11 = 1828
S_BUBSB1 = 1829
S_BUBSB2 = 1830
S_BUBSB3 = 1831
S_BUBSB4 = 1832
S_BUBSB5 = 1833
S_BUBSB6 = 1834
S_ZAPS1 = 1835
S_ZAPS2 = 1836
S_ZAPS3 = 1837
S_ZAPS4 = 1838
S_ZAPS5 = 1839
S_ZAPS6 = 1840
S_ZAPS7 = 1841
S_ZAPS8 = 1842
S_ZAPS9 = 1843
S_ZAPS10 = 1844
S_ZAPS11 = 1845
S_ZAPS12 = 1846
S_ZAPS13 = 1847
S_ZAPS14 = 1848
S_ZAPS15 = 1849
S_ZAPS16 = 1850
S_ZAPSB1 = 1851
S_ZAPSB2 = 1852
S_ZAPSB3 = 1853
S_ZAPSB4 = 1854
S_ZAPSB5 = 1855
S_ZAPSB6 = 1856
S_ZAPSB7 = 1857
S_ZAPSB8 = 1858
S_ZAPSB9 = 1859
S_ZAPSB10 = 1860
S_ZAPSB11 = 1861
S_THUNDERCOIN_SPARK = 1862
S_IVSP = 1863
S_SSPK1 = 1864
S_SSPK2 = 1865
S_SSPK3 = 1866
S_SSPK4 = 1867
S_SSPK5 = 1868
S_FLICKY_BUBBLE = 1869
S_FLICKY_01_OUT = 1870
S_FLICKY_01_FLAP1 = 1871
S_FLICKY_01_FLAP2 = 1872
S_FLICKY_01_FLAP3 = 1873
S_FLICKY_01_STAND = 1874
S_FLICKY_01_CENTER = 1875
S_FLICKY_02_OUT = 1876
S_FLICKY_02_AIM = 1877
S_FLICKY_02_HOP = 1878
S_FLICKY_02_UP = 1879
S_FLICKY_02_DOWN = 1880
S_FLICKY_02_STAND = 1881
S_FLICKY_02_CENTER = 1882
S_FLICKY_03_OUT = 1883
S_FLICKY_03_AIM = 1884
S_FLICKY_03_HOP = 1885
S_FLICKY_03_UP = 1886
S_FLICKY_03_FLAP1 = 1887
S_FLICKY_03_FLAP2 = 1888
S_FLICKY_03_STAND = 1889
S_FLICKY_03_CENTER = 1890
S_FLICKY_04_OUT = 1891
S_FLICKY_04_AIM = 1892
S_FLICKY_04_HOP = 1893
S_FLICKY_04_UP = 1894
S_FLICKY_04_DOWN = 1895
S_FLICKY_04_SWIM1 = 1896
S_FLICKY_04_SWIM2 = 1897
S_FLICKY_04_SWIM3 = 1898
S_FLICKY_04_SWIM4 = 1899
S_FLICKY_04_STAND = 1900
S_FLICKY_04_CENTER = 1901
S_FLICKY_05_OUT = 1902
S_FLICKY_05_AIM = 1903
S_FLICKY_05_HOP = 1904
S_FLICKY_05_UP = 1905
S_FLICKY_05_DOWN = 1906
S_FLICKY_05_STAND = 1907
S_FLICKY_05_CENTER = 1908
S_FLICKY_06_OUT = 1909
S_FLICKY_06_AIM = 1910
S_FLICKY_06_HOP = 1911
S_FLICKY_06_UP = 1912
S_FLICKY_06_DOWN = 1913
S_FLICKY_06_STAND = 1914
S_FLICKY_06_CENTER = 1915
S_FLICKY_07_OUT = 1916
S_FLICKY_07_AIML = 1917
S_FLICKY_07_HOPL = 1918
S_FLICKY_07_UPL = 1919
S_FLICKY_07_DOWNL = 1920
S_FLICKY_07_AIMR = 1921
S_FLICKY_07_HOPR = 1922
S_FLICKY_07_UPR = 1923
S_FLICKY_07_DOWNR = 1924
S_FLICKY_07_SWIM1 = 1925
S_FLICKY_07_SWIM2 = 1926
S_FLICKY_07_SWIM3 = 1927
S_FLICKY_07_STAND = 1928
S_FLICKY_07_CENTER = 1929
S_FLICKY_08_OUT = 1930
S_FLICKY_08_AIM = 1931
S_FLICKY_08_HOP = 1932
S_FLICKY_08_FLAP1 = 1933
S_FLICKY_08_FLAP2 = 1934
S_FLICKY_08_FLAP3 = 1935
S_FLICKY_08_FLAP4 = 1936
S_FLICKY_08_SWIM1 = 1937
S_FLICKY_08_SWIM2 = 1938
S_FLICKY_08_SWIM3 = 1939
S_FLICKY_08_SWIM4 = 1940
S_FLICKY_08_STAND = 1941
S_FLICKY_08_CENTER = 1942
S_FLICKY_09_OUT = 1943
S_FLICKY_09_AIM = 1944
S_FLICKY_09_HOP = 1945
S_FLICKY_09_UP = 1946
S_FLICKY_09_DOWN = 1947
S_FLICKY_09_STAND = 1948
S_FLICKY_09_CENTER = 1949
S_FLICKY_10_OUT = 1950
S_FLICKY_10_FLAP1 = 1951
S_FLICKY_10_FLAP2 = 1952
S_FLICKY_10_STAND = 1953
S_FLICKY_10_CENTER = 1954
S_FLICKY_11_OUT = 1955
S_FLICKY_11_AIM = 1956
S_FLICKY_11_RUN1 = 1957
S_FLICKY_11_RUN2 = 1958
S_FLICKY_11_RUN3 = 1959
S_FLICKY_11_STAND = 1960
S_FLICKY_11_CENTER = 1961
S_FLICKY_12_OUT = 1962
S_FLICKY_12_AIM = 1963
S_FLICKY_12_RUN1 = 1964
S_FLICKY_12_RUN2 = 1965
S_FLICKY_12_RUN3 = 1966
S_FLICKY_12_STAND = 1967
S_FLICKY_12_CENTER = 1968
S_FLICKY_13_OUT = 1969
S_FLICKY_13_AIM = 1970
S_FLICKY_13_HOP = 1971
S_FLICKY_13_UP = 1972
S_FLICKY_13_DOWN = 1973
S_FLICKY_13_STAND = 1974
S_FLICKY_13_CENTER = 1975
S_FLICKY_14_OUT = 1976
S_FLICKY_14_FLAP1 = 1977
S_FLICKY_14_FLAP2 = 1978
S_FLICKY_14_FLAP3 = 1979
S_FLICKY_14_STAND = 1980
S_FLICKY_14_CENTER = 1981
S_FLICKY_15_OUT = 1982
S_FLICKY_15_AIM = 1983
S_FLICKY_15_HOP = 1984
S_FLICKY_15_UP = 1985
S_FLICKY_15_DOWN = 1986
S_FLICKY_15_STAND = 1987
S_FLICKY_15_CENTER = 1988
S_FLICKY_16_OUT = 1989
S_FLICKY_16_FLAP1 = 1990
S_FLICKY_16_FLAP2 = 1991
S_FLICKY_16_FLAP3 = 1992
S_FLICKY_16_STAND = 1993
S_FLICKY_16_CENTER = 1994
S_SECRETFLICKY_01_OUT = 1995
S_SECRETFLICKY_01_AIM = 1996
S_SECRETFLICKY_01_HOP = 1997
S_SECRETFLICKY_01_UP = 1998
S_SECRETFLICKY_01_DOWN = 1999
S_SECRETFLICKY_01_STAND = 2000
S_SECRETFLICKY_01_CENTER = 2001
S_SECRETFLICKY_02_OUT = 2002
S_SECRETFLICKY_02_FLAP1 = 2003
S_SECRETFLICKY_02_FLAP2 = 2004
S_SECRETFLICKY_02_FLAP3 = 2005
S_SECRETFLICKY_02_STAND = 2006
S_SECRETFLICKY_02_CENTER = 2007
S_FAN = 2008
S_FAN2 = 2009
S_FAN3 = 2010
S_FAN4 = 2011
S_FAN5 = 2012
S_STEAM1 = 2013
S_STEAM2 = 2014
S_STEAM3 = 2015
S_STEAM4 = 2016
S_STEAM5 = 2017
S_STEAM6 = 2018
S_STEAM7 = 2019
S_STEAM8 = 2020
S_BUMPER = 2021
S_BUMPERHIT = 2022
S_BALLOON = 2023
S_BALLOONPOP1 = 2024
S_BALLOONPOP2 = 2025
S_BALLOONPOP3 = 2026
S_BALLOONPOP4 = 2027
S_BALLOONPOP5 = 2028
S_BALLOONPOP6 = 2029
S_YELLOWSPRING = 2030
S_YELLOWSPRING2 = 2031
S_YELLOWSPRING3 = 2032
S_YELLOWSPRING4 = 2033
S_YELLOWSPRING5 = 2034
S_REDSPRING = 2035
S_REDSPRING2 = 2036
S_REDSPRING3 = 2037
S_REDSPRING4 = 2038
S_REDSPRING5 = 2039
S_BLUESPRING = 2040
S_BLUESPRING2 = 2041
S_BLUESPRING3 = 2042
S_BLUESPRING4 = 2043
S_BLUESPRING5 = 2044
S_YDIAG1 = 2045
S_YDIAG2 = 2046
S_YDIAG3 = 2047
S_YDIAG4 = 2048
S_YDIAG5 = 2049
S_YDIAG6 = 2050
S_YDIAG7 = 2051
S_YDIAG8 = 2052
S_RDIAG1 = 2053
S_RDIAG2 = 2054
S_RDIAG3 = 2055
S_RDIAG4 = 2056
S_RDIAG5 = 2057
S_RDIAG6 = 2058
S_RDIAG7 = 2059
S_RDIAG8 = 2060
S_BDIAG1 = 2061
S_BDIAG2 = 2062
S_BDIAG3 = 2063
S_BDIAG4 = 2064
S_BDIAG5 = 2065
S_BDIAG6 = 2066
S_BDIAG7 = 2067
S_BDIAG8 = 2068
S_YHORIZ1 = 2069
S_YHORIZ2 = 2070
S_YHORIZ3 = 2071
S_YHORIZ4 = 2072
S_YHORIZ5 = 2073
S_YHORIZ6 = 2074
S_YHORIZ7 = 2075
S_YHORIZ8 = 2076
S_RHORIZ1 = 2077
S_RHORIZ2 = 2078
S_RHORIZ3 = 2079
S_RHORIZ4 = 2080
S_RHORIZ5 = 2081
S_RHORIZ6 = 2082
S_RHORIZ7 = 2083
S_RHORIZ8 = 2084
S_BHORIZ1 = 2085
S_BHORIZ2 = 2086
S_BHORIZ3 = 2087
S_BHORIZ4 = 2088
S_BHORIZ5 = 2089
S_BHORIZ6 = 2090
S_BHORIZ7 = 2091
S_BHORIZ8 = 2092
S_BOOSTERSOUND = 2093
S_YELLOWBOOSTERROLLER = 2094
S_YELLOWBOOSTERSEG_LEFT = 2095
S_YELLOWBOOSTERSEG_RIGHT = 2096
S_YELLOWBOOSTERSEG_FACE = 2097
S_REDBOOSTERROLLER = 2098
S_REDBOOSTERSEG_LEFT = 2099
S_REDBOOSTERSEG_RIGHT = 2100
S_REDBOOSTERSEG_FACE = 2101
S_RAIN1 = 2102
S_RAINRETURN = 2103
S_SNOW1 = 2104
S_SNOW2 = 2105
S_SNOW3 = 2106
S_SPLISH1 = 2107
S_SPLISH2 = 2108
S_SPLISH3 = 2109
S_SPLISH4 = 2110
S_SPLISH5 = 2111
S_SPLISH6 = 2112
S_SPLISH7 = 2113
S_SPLISH8 = 2114
S_SPLISH9 = 2115
S_LAVASPLISH = 2116
S_SPLASH1 = 2117
S_SPLASH2 = 2118
S_SPLASH3 = 2119
S_SMOKE1 = 2120
S_SMOKE2 = 2121
S_SMOKE3 = 2122
S_SMOKE4 = 2123
S_SMOKE5 = 2124
S_SMALLBUBBLE = 2125
S_MEDIUMBUBBLE = 2126
S_LARGEBUBBLE1 = 2127
S_LARGEBUBBLE2 = 2128
S_EXTRALARGEBUBBLE = 2129
S_POP1 = 2130
S_WATERZAP = 2131
S_SPINDUST1 = 2132
S_SPINDUST2 = 2133
S_SPINDUST3 = 2134
S_SPINDUST4 = 2135
S_SPINDUST_BUBBLE1 = 2136
S_SPINDUST_BUBBLE2 = 2137
S_SPINDUST_BUBBLE3 = 2138
S_SPINDUST_BUBBLE4 = 2139
S_SPINDUST_FIRE1 = 2140
S_SPINDUST_FIRE2 = 2141
S_SPINDUST_FIRE3 = 2142
S_SPINDUST_FIRE4 = 2143
S_FOG1 = 2144
S_FOG2 = 2145
S_FOG3 = 2146
S_FOG4 = 2147
S_FOG5 = 2148
S_FOG6 = 2149
S_FOG7 = 2150
S_FOG8 = 2151
S_FOG9 = 2152
S_FOG10 = 2153
S_FOG11 = 2154
S_FOG12 = 2155
S_FOG13 = 2156
S_FOG14 = 2157
S_SEED = 2158
S_PARTICLE = 2159
S_SCRA = 2160
S_SCRB = 2161
S_SCRC = 2162
S_SCRD = 2163
S_SCRE = 2164
S_SCRF = 2165
S_SCRG = 2166
S_SCRH = 2167
S_SCRI = 2168
S_SCRJ = 2169
S_SCRK = 2170
S_SCRL = 2171
S_ZERO1 = 2172
S_ONE1 = 2173
S_TWO1 = 2174
S_THREE1 = 2175
S_FOUR1 = 2176
S_FIVE1 = 2177
S_ZERO2 = 2178
S_ONE2 = 2179
S_TWO2 = 2180
S_THREE2 = 2181
S_FOUR2 = 2182
S_FIVE2 = 2183
S_FLIGHTINDICATOR = 2184
S_LOCKON1 = 2185
S_LOCKON2 = 2186
S_LOCKON3 = 2187
S_LOCKON4 = 2188
S_LOCKONINF1 = 2189
S_LOCKONINF2 = 2190
S_LOCKONINF3 = 2191
S_LOCKONINF4 = 2192
S_TTAG = 2193
S_GOTFLAG = 2194
S_FINISHFLAG = 2195
S_CORK = 2196
S_LHRT = 2197
S_RRNG1 = 2198
S_RRNG2 = 2199
S_RRNG3 = 2200
S_RRNG4 = 2201
S_RRNG5 = 2202
S_RRNG6 = 2203
S_RRNG7 = 2204
S_BOUNCERINGAMMO = 2205
S_RAILRINGAMMO = 2206
S_INFINITYRINGAMMO = 2207
S_AUTOMATICRINGAMMO = 2208
S_EXPLOSIONRINGAMMO = 2209
S_SCATTERRINGAMMO = 2210
S_GRENADERINGAMMO = 2211
S_BOUNCEPICKUP = 2212
S_BOUNCEPICKUPFADE1 = 2213
S_BOUNCEPICKUPFADE2 = 2214
S_BOUNCEPICKUPFADE3 = 2215
S_BOUNCEPICKUPFADE4 = 2216
S_BOUNCEPICKUPFADE5 = 2217
S_BOUNCEPICKUPFADE6 = 2218
S_BOUNCEPICKUPFADE7 = 2219
S_BOUNCEPICKUPFADE8 = 2220
S_RAILPICKUP = 2221
S_RAILPICKUPFADE1 = 2222
S_RAILPICKUPFADE2 = 2223
S_RAILPICKUPFADE3 = 2224
S_RAILPICKUPFADE4 = 2225
S_RAILPICKUPFADE5 = 2226
S_RAILPICKUPFADE6 = 2227
S_RAILPICKUPFADE7 = 2228
S_RAILPICKUPFADE8 = 2229
S_AUTOPICKUP = 2230
S_AUTOPICKUPFADE1 = 2231
S_AUTOPICKUPFADE2 = 2232
S_AUTOPICKUPFADE3 = 2233
S_AUTOPICKUPFADE4 = 2234
S_AUTOPICKUPFADE5 = 2235
S_AUTOPICKUPFADE6 = 2236
S_AUTOPICKUPFADE7 = 2237
S_AUTOPICKUPFADE8 = 2238
S_EXPLODEPICKUP = 2239
S_EXPLODEPICKUPFADE1 = 2240
S_EXPLODEPICKUPFADE2 = 2241
S_EXPLODEPICKUPFADE3 = 2242
S_EXPLODEPICKUPFADE4 = 2243
S_EXPLODEPICKUPFADE5 = 2244
S_EXPLODEPICKUPFADE6 = 2245
S_EXPLODEPICKUPFADE7 = 2246
S_EXPLODEPICKUPFADE8 = 2247
S_SCATTERPICKUP = 2248
S_SCATTERPICKUPFADE1 = 2249
S_SCATTERPICKUPFADE2 = 2250
S_SCATTERPICKUPFADE3 = 2251
S_SCATTERPICKUPFADE4 = 2252
S_SCATTERPICKUPFADE5 = 2253
S_SCATTERPICKUPFADE6 = 2254
S_SCATTERPICKUPFADE7 = 2255
S_SCATTERPICKUPFADE8 = 2256
S_GRENADEPICKUP = 2257
S_GRENADEPICKUPFADE1 = 2258
S_GRENADEPICKUPFADE2 = 2259
S_GRENADEPICKUPFADE3 = 2260
S_GRENADEPICKUPFADE4 = 2261
S_GRENADEPICKUPFADE5 = 2262
S_GRENADEPICKUPFADE6 = 2263
S_GRENADEPICKUPFADE7 = 2264
S_GRENADEPICKUPFADE8 = 2265
S_THROWNBOUNCE1 = 2266
S_THROWNBOUNCE2 = 2267
S_THROWNBOUNCE3 = 2268
S_THROWNBOUNCE4 = 2269
S_THROWNBOUNCE5 = 2270
S_THROWNBOUNCE6 = 2271
S_THROWNBOUNCE7 = 2272
S_THROWNINFINITY1 = 2273
S_THROWNINFINITY2 = 2274
S_THROWNINFINITY3 = 2275
S_THROWNINFINITY4 = 2276
S_THROWNINFINITY5 = 2277
S_THROWNINFINITY6 = 2278
S_THROWNINFINITY7 = 2279
S_THROWNAUTOMATIC1 = 2280
S_THROWNAUTOMATIC2 = 2281
S_THROWNAUTOMATIC3 = 2282
S_THROWNAUTOMATIC4 = 2283
S_THROWNAUTOMATIC5 = 2284
S_THROWNAUTOMATIC6 = 2285
S_THROWNAUTOMATIC7 = 2286
S_THROWNEXPLOSION1 = 2287
S_THROWNEXPLOSION2 = 2288
S_THROWNEXPLOSION3 = 2289
S_THROWNEXPLOSION4 = 2290
S_THROWNEXPLOSION5 = 2291
S_THROWNEXPLOSION6 = 2292
S_THROWNEXPLOSION7 = 2293
S_THROWNGRENADE1 = 2294
S_THROWNGRENADE2 = 2295
S_THROWNGRENADE3 = 2296
S_THROWNGRENADE4 = 2297
S_THROWNGRENADE5 = 2298
S_THROWNGRENADE6 = 2299
S_THROWNGRENADE7 = 2300
S_THROWNGRENADE8 = 2301
S_THROWNGRENADE9 = 2302
S_THROWNGRENADE10 = 2303
S_THROWNGRENADE11 = 2304
S_THROWNGRENADE12 = 2305
S_THROWNGRENADE13 = 2306
S_THROWNGRENADE14 = 2307
S_THROWNGRENADE15 = 2308
S_THROWNGRENADE16 = 2309
S_THROWNGRENADE17 = 2310
S_THROWNGRENADE18 = 2311
S_THROWNSCATTER = 2312
S_RINGEXPLODE = 2313
S_COIN1 = 2314
S_COIN2 = 2315
S_COIN3 = 2316
S_COINSPARKLE1 = 2317
S_COINSPARKLE2 = 2318
S_COINSPARKLE3 = 2319
S_COINSPARKLE4 = 2320
S_GOOMBA1 = 2321
S_GOOMBA1B = 2322
S_GOOMBA2 = 2323
S_GOOMBA3 = 2324
S_GOOMBA4 = 2325
S_GOOMBA5 = 2326
S_GOOMBA6 = 2327
S_GOOMBA7 = 2328
S_GOOMBA8 = 2329
S_GOOMBA9 = 2330
S_GOOMBA_DEAD = 2331
S_BLUEGOOMBA1 = 2332
S_BLUEGOOMBA1B = 2333
S_BLUEGOOMBA2 = 2334
S_BLUEGOOMBA3 = 2335
S_BLUEGOOMBA4 = 2336
S_BLUEGOOMBA5 = 2337
S_BLUEGOOMBA6 = 2338
S_BLUEGOOMBA7 = 2339
S_BLUEGOOMBA8 = 2340
S_BLUEGOOMBA9 = 2341
S_BLUEGOOMBA_DEAD = 2342
S_FIREFLOWER1 = 2343
S_FIREFLOWER2 = 2344
S_FIREFLOWER3 = 2345
S_FIREFLOWER4 = 2346
S_FIREBALL = 2347
S_FIREBALLTRAIL1 = 2348
S_FIREBALLTRAIL2 = 2349
S_SHELL = 2350
S_PUMA_START1 = 2351
S_PUMA_START2 = 2352
S_PUMA_UP1 = 2353
S_PUMA_UP2 = 2354
S_PUMA_UP3 = 2355
S_PUMA_DOWN1 = 2356
S_PUMA_DOWN2 = 2357
S_PUMA_DOWN3 = 2358
S_PUMATRAIL1 = 2359
S_PUMATRAIL2 = 2360
S_PUMATRAIL3 = 2361
S_PUMATRAIL4 = 2362
S_HAMMER = 2363
S_KOOPA1 = 2364
S_KOOPA2 = 2365
S_KOOPAFLAME1 = 2366
S_KOOPAFLAME2 = 2367
S_KOOPAFLAME3 = 2368
S_AXE1 = 2369
S_AXE2 = 2370
S_AXE3 = 2371
S_MARIOBUSH1 = 2372
S_MARIOBUSH2 = 2373
S_TOAD = 2374
S_NIGHTSDRONE_MAN1 = 2375
S_NIGHTSDRONE_MAN2 = 2376
S_NIGHTSDRONE_SPARKLING1 = 2377
S_NIGHTSDRONE_SPARKLING2 = 2378
S_NIGHTSDRONE_SPARKLING3 = 2379
S_NIGHTSDRONE_SPARKLING4 = 2380
S_NIGHTSDRONE_SPARKLING5 = 2381
S_NIGHTSDRONE_SPARKLING6 = 2382
S_NIGHTSDRONE_SPARKLING7 = 2383
S_NIGHTSDRONE_SPARKLING8 = 2384
S_NIGHTSDRONE_SPARKLING9 = 2385
S_NIGHTSDRONE_SPARKLING10 = 2386
S_NIGHTSDRONE_SPARKLING11 = 2387
S_NIGHTSDRONE_SPARKLING12 = 2388
S_NIGHTSDRONE_SPARKLING13 = 2389
S_NIGHTSDRONE_SPARKLING14 = 2390
S_NIGHTSDRONE_SPARKLING15 = 2391
S_NIGHTSDRONE_SPARKLING16 = 2392
S_NIGHTSDRONE_GOAL1 = 2393
S_NIGHTSDRONE_GOAL2 = 2394
S_NIGHTSDRONE_GOAL3 = 2395
S_NIGHTSDRONE_GOAL4 = 2396
S_NIGHTSPARKLE1 = 2397
S_NIGHTSPARKLE2 = 2398
S_NIGHTSPARKLE3 = 2399
S_NIGHTSPARKLE4 = 2400
S_NIGHTSPARKLESUPER1 = 2401
S_NIGHTSPARKLESUPER2 = 2402
S_NIGHTSPARKLESUPER3 = 2403
S_NIGHTSPARKLESUPER4 = 2404
S_NIGHTSLOOPHELPER = 2405
S_NIGHTSBUMPER1 = 2406
S_NIGHTSBUMPER2 = 2407
S_NIGHTSBUMPER3 = 2408
S_NIGHTSBUMPER4 = 2409
S_NIGHTSBUMPER5 = 2410
S_NIGHTSBUMPER6 = 2411
S_NIGHTSBUMPER7 = 2412
S_NIGHTSBUMPER8 = 2413
S_NIGHTSBUMPER9 = 2414
S_NIGHTSBUMPER10 = 2415
S_NIGHTSBUMPER11 = 2416
S_NIGHTSBUMPER12 = 2417
S_HOOP = 2418
S_HOOP_XMASA = 2419
S_HOOP_XMASB = 2420
S_NIGHTSCORE10 = 2421
S_NIGHTSCORE20 = 2422
S_NIGHTSCORE30 = 2423
S_NIGHTSCORE40 = 2424
S_NIGHTSCORE50 = 2425
S_NIGHTSCORE60 = 2426
S_NIGHTSCORE70 = 2427
S_NIGHTSCORE80 = 2428
S_NIGHTSCORE90 = 2429
S_NIGHTSCORE100 = 2430
S_NIGHTSCORE10_2 = 2431
S_NIGHTSCORE20_2 = 2432
S_NIGHTSCORE30_2 = 2433
S_NIGHTSCORE40_2 = 2434
S_NIGHTSCORE50_2 = 2435
S_NIGHTSCORE60_2 = 2436
S_NIGHTSCORE70_2 = 2437
S_NIGHTSCORE80_2 = 2438
S_NIGHTSCORE90_2 = 2439
S_NIGHTSCORE100_2 = 2440
S_NIGHTSSUPERLOOP = 2441
S_NIGHTSDRILLREFILL = 2442
S_NIGHTSHELPER = 2443
S_NIGHTSEXTRATIME = 2444
S_NIGHTSLINKFREEZE = 2445
S_EGGCAPSULE = 2446
S_ORBITEM1 = 2447
S_ORBITEM2 = 2448
S_ORBITEM3 = 2449
S_ORBITEM4 = 2450
S_ORBITEM5 = 2451
S_ORBITEM6 = 2452
S_ORBITEM7 = 2453
S_ORBITEM8 = 2454
S_ORBIDYA1 = 2455
S_ORBIDYA2 = 2456
S_ORBIDYA3 = 2457
S_ORBIDYA4 = 2458
S_ORBIDYA5 = 2459
S_NIGHTOPIANHELPER1 = 2460
S_NIGHTOPIANHELPER2 = 2461
S_NIGHTOPIANHELPER3 = 2462
S_NIGHTOPIANHELPER4 = 2463
S_NIGHTOPIANHELPER5 = 2464
S_NIGHTOPIANHELPER6 = 2465
S_NIGHTOPIANHELPER7 = 2466
S_NIGHTOPIANHELPER8 = 2467
S_NIGHTOPIANHELPER9 = 2468
S_PIAN_LOOK1 = 2469
S_PIAN_LOOK2 = 2470
S_PIAN_LOOK3 = 2471
S_PIAN_FLY1 = 2472
S_PIAN_FLY2 = 2473
S_PIAN_FLY3 = 2474
S_PIAN_SING = 2475
S_SHLEEP1 = 2476
S_SHLEEP2 = 2477
S_SHLEEP3 = 2478
S_SHLEEP4 = 2479
S_SHLEEPBOUNCE1 = 2480
S_SHLEEPBOUNCE2 = 2481
S_SHLEEPBOUNCE3 = 2482
S_PENGUINATOR_LOOK = 2483
S_PENGUINATOR_WADDLE1 = 2484
S_PENGUINATOR_WADDLE2 = 2485
S_PENGUINATOR_WADDLE3 = 2486
S_PENGUINATOR_WADDLE4 = 2487
S_PENGUINATOR_SLIDE1 = 2488
S_PENGUINATOR_SLIDE2 = 2489
S_PENGUINATOR_SLIDE3 = 2490
S_PENGUINATOR_SLIDE4 = 2491
S_PENGUINATOR_SLIDE5 = 2492
S_POPHAT_LOOK = 2493
S_POPHAT_SHOOT1 = 2494
S_POPHAT_SHOOT2 = 2495
S_POPHAT_SHOOT3 = 2496
S_POPHAT_SHOOT4 = 2497
S_POPSHOT = 2498
S_POPSHOT_TRAIL = 2499
S_HIVEELEMENTAL_LOOK = 2500
S_HIVEELEMENTAL_PREPARE1 = 2501
S_HIVEELEMENTAL_PREPARE2 = 2502
S_HIVEELEMENTAL_SHOOT1 = 2503
S_HIVEELEMENTAL_SHOOT2 = 2504
S_HIVEELEMENTAL_DORMANT = 2505
S_HIVEELEMENTAL_PAIN = 2506
S_HIVEELEMENTAL_DIE1 = 2507
S_HIVEELEMENTAL_DIE2 = 2508
S_HIVEELEMENTAL_DIE3 = 2509
S_BUMBLEBORE_SPAWN = 2510
S_BUMBLEBORE_LOOK1 = 2511
S_BUMBLEBORE_LOOK2 = 2512
S_BUMBLEBORE_FLY1 = 2513
S_BUMBLEBORE_FLY2 = 2514
S_BUMBLEBORE_RAISE = 2515
S_BUMBLEBORE_FALL1 = 2516
S_BUMBLEBORE_FALL2 = 2517
S_BUMBLEBORE_STUCK1 = 2518
S_BUMBLEBORE_STUCK2 = 2519
S_BUMBLEBORE_DIE = 2520
S_BUGGLEIDLE = 2521
S_BUGGLEFLY = 2522
S_SMASHSPIKE_FLOAT = 2523
S_SMASHSPIKE_EASE1 = 2524
S_SMASHSPIKE_EASE2 = 2525
S_SMASHSPIKE_FALL = 2526
S_SMASHSPIKE_STOMP1 = 2527
S_SMASHSPIKE_STOMP2 = 2528
S_SMASHSPIKE_RISE1 = 2529
S_SMASHSPIKE_RISE2 = 2530
S_CACO_LOOK = 2531
S_CACO_WAKE1 = 2532
S_CACO_WAKE2 = 2533
S_CACO_WAKE3 = 2534
S_CACO_WAKE4 = 2535
S_CACO_ROAR = 2536
S_CACO_CHASE = 2537
S_CACO_CHASE_REPEAT = 2538
S_CACO_RANDOM = 2539
S_CACO_PREPARE_SOUND = 2540
S_CACO_PREPARE1 = 2541
S_CACO_PREPARE2 = 2542
S_CACO_PREPARE3 = 2543
S_CACO_SHOOT_SOUND = 2544
S_CACO_SHOOT1 = 2545
S_CACO_SHOOT2 = 2546
S_CACO_CLOSE = 2547
S_CACO_DIE_FLAGS = 2548
S_CACO_DIE_GIB1 = 2549
S_CACO_DIE_GIB2 = 2550
S_CACO_DIE_SCREAM = 2551
S_CACO_DIE_SHATTER = 2552
S_CACO_DIE_FALL = 2553
S_CACOSHARD_RANDOMIZE = 2554
S_CACOSHARD1_1 = 2555
S_CACOSHARD1_2 = 2556
S_CACOSHARD2_1 = 2557
S_CACOSHARD2_2 = 2558
S_CACOFIRE1 = 2559
S_CACOFIRE2 = 2560
S_CACOFIRE3 = 2561
S_CACOFIRE_EXPLODE1 = 2562
S_CACOFIRE_EXPLODE2 = 2563
S_CACOFIRE_EXPLODE3 = 2564
S_CACOFIRE_EXPLODE4 = 2565
S_SPINBOBERT_MOVE_FLIPUP = 2566
S_SPINBOBERT_MOVE_UP = 2567
S_SPINBOBERT_MOVE_FLIPDOWN = 2568
S_SPINBOBERT_MOVE_DOWN = 2569
S_SPINBOBERT_FIRE_MOVE = 2570
S_SPINBOBERT_FIRE_GHOST = 2571
S_SPINBOBERT_FIRE_TRAIL1 = 2572
S_SPINBOBERT_FIRE_TRAIL2 = 2573
S_SPINBOBERT_FIRE_TRAIL3 = 2574
S_HANGSTER_LOOK = 2575
S_HANGSTER_SWOOP1 = 2576
S_HANGSTER_SWOOP2 = 2577
S_HANGSTER_ARC1 = 2578
S_HANGSTER_ARC2 = 2579
S_HANGSTER_ARC3 = 2580
S_HANGSTER_FLY1 = 2581
S_HANGSTER_FLY2 = 2582
S_HANGSTER_FLY3 = 2583
S_HANGSTER_FLY4 = 2584
S_HANGSTER_FLYREPEAT = 2585
S_HANGSTER_ARCUP1 = 2586
S_HANGSTER_ARCUP2 = 2587
S_HANGSTER_ARCUP3 = 2588
S_HANGSTER_RETURN1 = 2589
S_HANGSTER_RETURN2 = 2590
S_HANGSTER_RETURN3 = 2591
S_CRUMBLE1 = 2592
S_CRUMBLE2 = 2593
S_SPRK1 = 2594
S_SPRK2 = 2595
S_SPRK3 = 2596
S_XPLD_FLICKY = 2597
S_XPLD1 = 2598
S_XPLD2 = 2599
S_XPLD3 = 2600
S_XPLD4 = 2601
S_XPLD5 = 2602
S_XPLD6 = 2603
S_XPLD_EGGTRAP = 2604
S_WPLD1 = 2605
S_WPLD2 = 2606
S_WPLD3 = 2607
S_WPLD4 = 2608
S_WPLD5 = 2609
S_WPLD6 = 2610
S_DUST1 = 2611
S_DUST2 = 2612
S_DUST3 = 2613
S_DUST4 = 2614
S_ROCKSPAWN = 2615
S_ROCKCRUMBLEA = 2616
S_ROCKCRUMBLEB = 2617
S_ROCKCRUMBLEC = 2618
S_ROCKCRUMBLED = 2619
S_ROCKCRUMBLEE = 2620
S_ROCKCRUMBLEF = 2621
S_ROCKCRUMBLEG = 2622
S_ROCKCRUMBLEH = 2623
S_ROCKCRUMBLEI = 2624
S_ROCKCRUMBLEJ = 2625
S_ROCKCRUMBLEK = 2626
S_ROCKCRUMBLEL = 2627
S_ROCKCRUMBLEM = 2628
S_ROCKCRUMBLEN = 2629
S_ROCKCRUMBLEO = 2630
S_ROCKCRUMBLEP = 2631
S_GFZDEBRIS = 2632
S_BRICKDEBRIS = 2633
S_WOODDEBRIS = 2634
S_REDBRICKDEBRIS = 2635
S_BLUEBRICKDEBRIS = 2636
S_YELLOWBRICKDEBRIS = 2637
S_NAMECHECK = 2638
