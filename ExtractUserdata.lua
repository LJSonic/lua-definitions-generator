local parsePage = require 'parsePage'
local extractFunction = require 'extractFunction'
local util = require 'util'
local re = require 're'
local lpeg = require 'lpeg'

local err = util.printerr

local manualFieldTypes = {
	['sector_t.ffloors'] = 'fun(): fun(): ffloor_t',
}

local descriptions = {
	mobj_t = [[
		This userdata type represents an Object. In the examples below, mobj is used as the name of the mobj_t variable. An access to a variable var of mobj_t is written as mobj.var.

		Accessibility: Read+Write
		Allows custom variables: Yes
	]]
}

local enumTypes = {
	AST_       = 'integer',
	FF_        = 'ffloorflags_t',
	MSF_       = 'sectorflags_t',
	MT_        = 'mobjtype_t',
	PA_        = 'playeranim_t',
	PF_        = 'playerflags_t',
	PST_       = 'playerstate_t',
	ROTAXIS_   = 'rotaxis_t',
	S_         = 'statenum_t',
	SD_        = 'sectordamage_t',
	SF_        = 'skinflags_t',
	sfx_       = 'soundnum_t',
	SKINCOLOR_ = 'skincolornum_t',
	SPR_       = 'spritenum_t',
	SSF_       = 'sectorspecialflags_t',
}

local doc = {}

local function appendDoc(s)
	table.insert(doc, s)
end

local function extractFieldName(cell)
	local pattern = re.compile([[
		Name <- Id (Sp '[' (. !']' )* ']')?
		Id   <- Sp { [a-zA-Z0-9_]+ }
		Sp   <- %s*
	]])

	return lpeg.match(pattern, util.wikiToPlainString(cell.content))
end

local function extractFieldType(cell)
	local pattern = re.compile([[
		Type <- { 'N/A' } / Id (Sp '(' Id '*' Sp ')' / { '' }) (Sp { 'array' })?
		Id   <- Sp { [a-zA-Z0-9_]+ }
		Sp   <- %s*
	]])

	local fieldType, enumPrefix, isArray = lpeg.match(pattern, util.wikiToPlainString(cell.content))

	if fieldType == 'enum' then
		fieldType = enumTypes[enumPrefix]
	end

	if isArray == 'array' and fieldType then
		fieldType = fieldType .. '[]'
	end

	return fieldType
end

local function extractUserdataFields(t, userdataName)
	local description = descriptions[userdataName]
	if description then
		appendDoc(
			'---\n' ..
			util.prefixLines(description:gsub('\t', ''), '---') ..
			'---\n'
		)
	end

	userdataName = userdataName:gsub('*$', '_t')

	appendDoc(
		'---@class ' ..
		userdataName ..
		'\n'
	)

	for i = 3, #t.rows do
		local row = t.rows[i]

		local name = extractFieldName(row.cells[1])

		local fieldType = manualFieldTypes[userdataName .. '.' .. name] or extractFieldType(row.cells[2])
		if fieldType then
			local access = util.wikiToMarkdown(row.cells[3].content, 'Userdata_structures')
			access = 'Accessibility:' .. (access:find('\n') and '\n' or ' ') .. access
			access = util.prefixLines(access, '---')

			if fieldType ~= 'N/A' and access ~= 'None' then
				appendDoc(
					'---\n' ..
					'---\n' ..
					access ..
					'---\n' ..
					util.extractDescription(row.cells[4].content, 'Userdata_structures', 'Lua/Userdata_structures#' .. userdataName) ..
					'---\n' ..
					'---@field ' .. name .. ' ' .. fieldType .. '\n'
				)
			end
		else
			err('Failed to parse type for field ' .. name .. ' of userdata ' .. userdataName .. ':')
			err(util.wikiToPlainString(row.cells[2].content))
			err()
		end
	end

	appendDoc('local ' .. userdataName .. ' = {}\n\n')
end

local page = parsePage(util.readFile('wiki/Userdata_structures.txt'))

for _, t in ipairs(page) do
	local headers = t.rows[1]
	if headers then
		local cell = headers.cells[1]
		local text = util.wikiToPlainString(cell.content)

		local name, type = text:match('^([^%s]+) (.+)$')

		if type == 'structure' or type == 'does not have any attributes.' then
			extractUserdataFields(t, name)
		elseif type == 'methods' or type == 'functions' then
			for i = 3, #t.rows do
				appendDoc(extractFunction(t.rows[i], name))
			end
		end
	end
end

doc = table.concat(doc):gsub('\n*$', '\n')
util.writeFile('defs/Userdata.lua', doc)
