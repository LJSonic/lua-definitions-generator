-- Todo:
-- Value (Hex)

local parsePage = require 'parsePage'
local util = require 'util'

local err = util.printerr

local manualConstantNames = {
}

local manualConstantValues = {
	FRACUNIT     = '65535',
	FU           = '65535',
	NEWTICRATE   = '35',
	RING_DIST    = 'nil',
	PUSHACCEL    = 'nil',
	FLOATSPEED   = 'nil',
	MAXSTEPMOVE  = 'nil',
	USERANGE     = 'nil',
	MELEERANGE   = 'nil',
	MISSILERANGE = 'nil',
	ONFLOORZ     = 'nil',
	ONCEILINGZ   = 'nil',
	INFLIVES     = '127',

	LE_BRAKVILEATACK = '-6',
	LE_BOSS4DROP     = '-5',
	LE_BOSSDEAD      = '-4',
	LE_ALLBOSSESDEAD = '-3',
	LE_PINCHPHASE    = '-2',
}

local manualConstantTypes = {
	FRACUNIT     = 'fixed_t',
	FU           = 'fixed_t',
	TICRATE      = 'tic_t',
	NEWTICRATE   = 'tic_t',
	RING_DIST    = 'fixed_t',
	PUSHACCEL    = 'fixed_t',
	FLOATSPEED   = 'fixed_t',
	MAXSTEPMOVE  = 'fixed_t',
	USERANGE     = 'fixed_t',
	MELEERANGE   = 'fixed_t',
	MISSILERANGE = 'fixed_t',
	ONFLOORZ     = 'fixed_t',
	ONCEILINGZ   = 'fixed_t',
}

local ignoredConstants = util.arrayToSet{
	'numskincolors',
}

local nameHeaders = util.arrayToSet{
	'Name',
	'Internal name',
	'Flag name',
	'SOC/Lua Constant',
	'Mask name',
	'Object type',
	'Palette name',
	'HUD item name',
	'Sprite name',
	'Sound name',
}

local valueHeaders = util.arrayToSet{
	'Value',
	'Value (Hex)',
	'Decimal',
	'Object type number',
	'Palette number',
	'Tag',
	'Mask value (Decimal)',
	'ID',
	'Sprite number',
	'Sound number',
}

local descriptionHeaders = util.arrayToSet{
	'Description',
	'String name',
}

local doc = {}

local function appendDoc(s)
	table.insert(doc, s)
end

local function findRowIndexes(t)
	local headers = t.rows[1]
	if not headers then
		return
	end

	local nameIndex, valueIndex, descriptionIndex

	for cellIndex, cell in ipairs(headers.cells) do
		local text = util.wikiToPlainString(cell.content):gsub('\n', ' ')

		if nameHeaders[text] then
			nameIndex = cellIndex
		elseif valueHeaders[text] then
			valueIndex = cellIndex
		elseif descriptionHeaders[text] then
			descriptionIndex = cellIndex
		end
	end

	return nameIndex, valueIndex, descriptionIndex
end

local function extractConstant(row, nameIndex, valueIndex, descriptionIndex, pageName)
	local nameCell        = row.cells[nameIndex]
	local valueCell       = row.cells[valueIndex]
	local descriptionCell = row.cells[descriptionIndex]
	if not (nameCell and valueCell and descriptionCell) then
		return
	end

	local names = util.wikiToPlainString(nameCell.content)

	if names == 'n/a'
	or names == "none"
	or ignoredConstants[names:match('[%w_]+')] then
		return
	end

	for name in names:gmatch("[^%\n]+") do
		name = manualConstantNames[name:match('[%w_]+')] or name

		if not name:match('^[%w_]+%s*$') then
			err('Failed to parse constant name:')
			err(name)
			err()
			return
		end

		local value = manualConstantValues[name]
		if not value then
			value = util.wikiToPlainString(valueCell.content)
			if not (value:match('^[%d-]+$') or value:match('^".*"$')) then
				err('Failed to parse value for constant ' .. name .. ':')
				err(value)
				err()
			end
		end

		local description = util.extractDescription(descriptionCell.content, pageName, pageName)

		local constantType = manualConstantTypes[name] or 'integer'
		if constantType ~= 'integer' then
			appendDoc('---@type ' .. constantType .. '\n')
		end

		appendDoc(
			description ..
			name .. ' = ' .. value .. '\n'
		)
	end
end

local function extractConstants(t, fileName)
	local nameIndex, valueIndex, descriptionIndex = findRowIndexes(t)
	if not (nameIndex and valueIndex and descriptionIndex) then
		return
	end

	for i = 2, #t.rows do
		extractConstant(t.rows[i], nameIndex, valueIndex, descriptionIndex, fileName)
	end

	appendDoc('\n')
end

local function extractObjects(t)
	local value = 0

	for i = 2, #t.rows do
		local cells = t.rows[i].cells
		if #cells == 3 then
			local name = util.wikiToPlainString(cells[1].content)
			if name:sub(1, 3) == "MT_" then
				local description = util.extractDescription(cells[2].content, 'List_of_Object_types', 'List_of_Object_types')

				appendDoc(
					description ..
					name .. ' = ' .. value .. '\n'
				)

				value = value + 1
			end
		end
	end

	appendDoc('\n')
end

for _, pageName in ipairs{
	'Constants',
	'Object_flags',
	'Thing',
	'List_of_sprites',
	'List_of_sounds',
	'Sound',
	'S_SKIN',
	'Powers',
	'Palette',
	'Linedef',
	'Reserved_tags',
	'Linedef_type_259',
	'Video_flags',
	'Head-up_display',
	'List_of_skin_colors',
} do
	local page = parsePage(util.readFile('wiki/' .. pageName .. '.txt'))
	for _, t in ipairs(page) do
		extractConstants(t, pageName)
	end
end

local page = parsePage(util.readFile('wiki/List_of_Object_types.txt'))
for _, t in ipairs(page) do
	local headers = t.rows[1]
	if headers then
		local cell = headers.cells[1]
		if util.wikiToPlainString(cell.content) == 'Object type' then
			extractObjects(t)
		end
	end
end

doc = table.concat(doc):gsub('\n*$', '\n')
util.writeFile('defs/Constants.lua', doc)
