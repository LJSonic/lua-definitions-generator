local util = require 'util'
local lpeg = require 'lpeg'
local re = require 're'

local err = util.printerr

local manualParamTypes = {
	addHook = { hook = 'hooktype' },

	["hud.add"] = { hook = 'hudtype' },
	["hud.enable"] = { huditem = 'huditemtype' },
	["hud.disable"] = { huditem = 'huditemtype' },
	["hud.enabled"] = { huditem = 'huditemtype' },

	["v.drawString"] = { align = 'stringtype' },
	["v.stringWidth"] = { widthtype = 'stringwidthtype' },
}

local manualReturnTypes = {
	dofile = '---@return ... any',
	freeslot = '---@return ... int',
	reserveLuabanks = '---@return UINT32[]',
	['file_t:read'] = '---@return ... any',
	['mobjs.iterate'] = '---@return fun(): mobj_t',
}

local ignoredFunctions = util.arrayToSet{
	'assert',
	'collectgarbage',
	'error',
	'gcinfo',
	'getfenv',
	'getmetatable',
	'ipairs',
	'next',
	'pairs',
	'pcall',
	'rawequal',
	'rawget',
	'rawset',
	'select',
	'setfenv',
	'setmetatable',
	'tonumber',
	'tostring',
	'type',
	'unpack',
	'xpcall',

	'coroutine.create',
	'coroutine.resume',
	'coroutine.running',
	'coroutine.status',
	'coroutine.wrap',
	'coroutine.yield',

	'string.byte',
	'string.char',
	'string.find',
	'string.format',
	'string.gmatch',
	'string.gsub',
	'string.len',
	'string.lower',
	'string.match',
	'string.rep',
	'string.reverse',
	'string.sub',
	'string.upper',

	'table.concat',
	'table.insert',
	'table.maxn',
	'table.remove',
	'table.sort',

	'io.close',
	'io.tmpfile',
	'io.type',

	'file_t:close',
	'file_t:flush',
	'file_t:setvbuf',
	'file_t:write',
}

local function getTypeUnionAsString(type)
	for i = 1, #type do
		type[i] = type[i]:gsub('^int$', 'integer')
	end

	return table.concat(type, '|')
end

local function extractNameAndParams(cell)
	local optional = 0
	local prevType = 'any'

	local pattern = re.compile([[
		Signature <- (IdIgn Sp ':')? Id (Sp '(' {| (OptStart* Comma Param OptEnd*)* |} Sp ')')?

		Param  <- Sp '...' => VarArg / (Type Id '?'?) => Param
		Type   <- {| TypeId (Sp '/' TypeId)* |}
		TypeId <- Sp '*' -> 'any' / Id ('{' [^}]* '}' / '(' [^)]* ')')?

		OptStart <- Comma '[' => OptStart
		OptEnd   <- Comma ']' => OptEnd

		Id    <- Sp { [a-zA-Z0-9_.]+ '*'? }
		IdIgn <- Sp [a-zA-Z0-9_]+ '*'?
		Comma <- (%s / ',')*
		Sp    <- %s*
	]], {
		Param = function(_, _, type, name)
			type = getTypeUnionAsString(type)
			prevType = type

			return true, {
				type = type,
				name = name,
				optional = (optional ~= 0),
				vararg = false
			}
		end,

		VarArg = function()
			return true, {
				type = prevType,
				name = '...',
				optional = (optional ~= 0),
				vararg = true
			}
		end,

		OptStart = function()
			optional = optional + 1
			return true
		end,

		OptEnd = function()
			optional = optional - 1
			return true
		end,
	})

	return lpeg.match(pattern, util.wikiToPlainString(cell.content))
end

local function extractReturnType(cell)
	local pattern = re.compile([[
		Types <- {| Type (Sp ',' Type)* Sp !. |}
		Type  <- {| Id (Sp 'or' Id)* |} -> Type
		Id    <- Sp { [a-zA-Z0-9_]+ '*'? '[]'? }
		Sp    <- %s*
	]], {
		Type = function(type)
			return getTypeUnionAsString(type)
		end
	})

	return lpeg.match(pattern, util.wikiToPlainString(cell.content))
end

return function(row, userdataName, pageName)
	local doc = {}

	local function appendDoc(s)
		table.insert(doc, s)
	end

	local name, params = extractNameAndParams(row.cells[1])
	if not name then
		err('Failed to parse function signature:')
		err(util.wikiToPlainString(row.cells[1].content))
		err()
		return
	end

	if userdataName then
		name = userdataName:gsub('*$', '_t') .. ':' .. name
	end

	if ignoredFunctions[name] then
		return
	end

	if not params then
		err('Failed to parse signature for function ' .. name)
		err()
		return
	end

	local returnTypes = extractReturnType(row.cells[2])
	if not (returnTypes or manualReturnTypes[name]) then
		err('Failed to parse return type for function ' .. name .. ':')
		err('"' .. util.wikiToPlainString(row.cells[2].content) .. '"')
		err()
		return
	end

	local wikiLink = userdataName and
		'Lua/Userdata_structures#' .. userdataName
		or
		'Lua/Functions#' .. name

	appendDoc(
		'---\n' ..
		util.extractDescription(row.cells[3].content, pageName, wikiLink) ..
		'---\n'
	)

	for i, param in ipairs(params) do
		if param.vararg then
			appendDoc('---@vararg ' .. param.type .. '\n')
		else
			local type = manualParamTypes[name] and manualParamTypes[name][param.name] or param.type

			appendDoc(
				'---@param ' .. param.name ..
				(param.optional and '? ' or ' ') ..
				type .. '\n'
			)
		end
	end

	if manualReturnTypes[name] then
		appendDoc(manualReturnTypes[name] .. '\n')
	else
		for _, type in ipairs(returnTypes) do
			appendDoc('---@return ' .. type .. '\n')
		end
	end

	appendDoc('function ' .. name .. '(')
	local first = true
	for _, param in ipairs(params) do
		if first then
			first = false
			appendDoc(param.name)
		else
			appendDoc(', ' .. param.name)
		end
	end
	appendDoc(') end\n\n')

	return(table.concat(doc))
end
