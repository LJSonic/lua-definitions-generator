local util = {}

function util.arrayToSet(array)
	local set = {}
	for _, key in ipairs(array) do
		set[key] = true
	end
	return set
end

function util.printerr(...)
	local args = {}
	for _, arg in ipairs({...}) do
		table.insert(args, tostring(arg))
	end

	io.stderr:write(table.concat(args, '\t') .. '\n')
end

function util.dump(t, spaces, done)
	spaces = spaces or ''
	done = done or {}

	if done[t] then
		return
	end
	done[t] = true

	print(spaces .. '{')

	for k, v in pairs(t) do
		if type(v) == 'table' then
			print(spaces .. '    ' .. tostring(k) .. ':')
			util.dump(v, spaces .. '    ')
		elseif type(v) == 'string' then
			print(spaces .. '    ' .. tostring(k) .. ': "' .. v:gsub('\n', '\\n') .. '"')
		else
			print(spaces .. '    ' .. tostring(k) .. ': ' .. tostring(v))
		end
	end

	print(spaces .. '}')
end

function util.wikiToPlainString(content)
	local s = {}

	for _, part in ipairs(content) do
		if part.type == 'Text' then
			table.insert(s, part.content)
		elseif part.type == 'Element' then
			if part.startTag.name == 'br' then
				table.insert(s, '\n')
			elseif part.startTag.name ~= 'sup' then
				table.insert(s, util.wikiToPlainString(part.content))
			end
		elseif part.type ~= 'Table'
		and not (part.type == 'Template' and part.target == 'anchor')
		and part.content then
			table.insert(s, util.wikiToPlainString(part.content))
		end
	end

	return table.concat(s)
end

local function parseWikiLink(link, pageName)
	local base
	if link:sub(1, 2) == 'w:' then
		base = 'https://en.wikipedia.org/wiki/'
		link = link:sub(3)
	else
		base = 'https://wiki.srb2.org/wiki/'
	end

	if link:sub(1, 1) == '#' then
		link = base .. pageName .. link
	else
		link = base .. link
	end

	local link = link:gsub(' ', '_')

	return link
end

function util.wikiToMarkdown(content, pageName)
	local s = {}

	for _, part in ipairs(content) do
		local text

		local t = part.type
		if t == 'Text' then
			text = part.content
		elseif t == 'Element' then
			if part.startTag.name == 'br' then
				text = '\n'
			elseif part.startTag.name == 'code' then
				text = util.wikiToMarkdown(part.content, pageName)
				local child = part.content[1]
				if not (child and child.type == 'Link') then
					text = '`' .. text .. '`'
				end
			elseif part.startTag.name == 'source' or part.startTag.name == 'syntaxhighlight' then
				local lang = part.startTag.attrs[1]
				text =
					'```' .. (lang and lang.value or '') .. '\n' ..
					util.wikiToMarkdown(part.content, pageName) .. '\n' ..
					'```'
			elseif part.startTag.name ~= 'sup' then
				text = util.wikiToMarkdown(part.content, pageName)
			end
		elseif t == 'Link' then
			text =
				'[' .. util.wikiToMarkdown(part.content, pageName) .. ']' ..
				'(' .. parseWikiLink(part.target, pageName) .. ')'
		elseif t ~= 'Table'
		and not (t == 'Template' and part.target == 'anchor')
		and part.content then
			text = util.wikiToMarkdown(part.content, pageName)
		end

		if text then
			table.insert(s, text)
		end
	end

	return table.concat(s)
end

function util.prefixLines(s, prefix)
	return s
		:gsub('[^\n]$', '%0\n')
		:gsub('.-\n', prefix .. '%0')
end

function util.extractDescription(description, pageName, wikiLink, prefix)
	prefix = prefix or '---'

	description = util.wikiToMarkdown(description, pageName)
	description = util.prefixLines(description, prefix)

	if wikiLink then
		description =
			prefix .. '**[View on wiki](https://wiki.srb2.org/wiki/' .. wikiLink .. ')**\n' ..
			prefix .. '\n' ..
			description
	end

	return description
end

function util.readFile(path)
	local file = io.open(path, 'r')
	local data = file:read('*a')
	file:close()

	return data
end

function util.writeFile(path, data)
	local file = io.open(path, 'w')
	file:write(data)
	file:close()
end

return util
