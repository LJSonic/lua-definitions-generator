-- By LJ Sonic

local util = require 'util'

require 'ExtractUserdata'
require 'ExtractFunctions'
require 'ExtractVariables'
require 'ExtractConstants'

local file = io.open('defs/defs.lua', 'w')

for _, path in ipairs{
	'Manual.lua',
	'defs/functions.lua',
	'defs/userdata.lua',
	'defs/variables.lua',
	'defs/constants.lua',
} do
	file:write(util.readFile(path))
	file:write('\n\n')
end

file:close()
