local lpeg = require 'lpeg'
local re = require 're'

local specialChars = {
	nbsp  = ' ',
	ndash = '-',
}

local grammar = re.compile([=[
	Page <- {| ((!Table .)* Table)* |}

	NonText <- Code / Element / Bold / Italic / Link / Template / Table

	Bold        <- "'''" BoldContent -> Bold "'''"
	BoldContent <- {| (BoldText / !"'''" NonText)* |}
	BoldText    <- {| ({ (!("'''" / NonText / SpecialChar) .)+ } / SpecialChar)+ |} -> Text

	Italic        <- "''" ItalicContent -> Italic "''"
	ItalicContent <- {| (ItalicText / !("''" !"'" / "'''''") NonText)* |}
	ItalicText    <- {| ({ (!("''" / NonText / SpecialChar) .)+ } / SpecialChar)+ |} -> Text

	Link        <- ('[[' LinkTarget LinkContent? ']]')
	            -> Link
	LinkTarget  <- { (!(']]' / '|') .)* }
	LinkContent <- '|' {| (LinkText / NonText)* |}
	LinkText    <- {| ({ (!(']]' / NonText / SpecialChar) .)+ } / SpecialChar)+ |} -> Text

	Template        <- ('{{' TemplateTarget TemplateContent? (!'}}' .)* '}}')
	                -> Template
	TemplateTarget  <- { (!('}}' / '|') .)* }
	TemplateContent <- '|' {| (TemplateText / NonText)* |}
	TemplateText    <- {| ({ (!('}}' / '|' / NonText / SpecialChar) .)+ } / SpecialChar)+ |} -> Text

	Element        <- (SelfClosingTag {| |} / StartTag ElementContent EndTag)
	               -> Element
	StartTag       <- ('<' Identifier {| Attr* |} Blank '>')
	               -> StartTag
	EndTag         <- '</' Identifier -> EndTag Blank '>'
	SelfClosingTag <- ('<' Identifier {| Attr* |} Blank '/' Blank '>' / Blank '<br>')
	               -> StartTag
	Attr           <- (Identifier Blank '=' Blank '"' { (!('"' / %nl) .)* } '"')
	               -> Attr

	Code         <- (StartCodeTag CodeContent EndCodeTag)
	             -> Element
	StartCodeTag <- ('<' { 'code' } '>' {| |})
	             -> StartTag
	EndCodeTag   <- '</' { 'code' } -> EndTag '>'

	CodeContent  <- {| NonText? (CodeText / NonCodeText)* |}
	CodeText     <- {| ({ (!(EndCodeTag / NonCodeText / SpecialChar) .)+ } / SpecialChar)+ |} -> Text
	NonCodeText  <- Element / Bold / Italic

	ElementContent <- {| (ElementText / NonText)* |}
	ElementText    <- {| ({ (!(EndTag / NonText / SpecialChar) .)+ } / SpecialChar)+ |} -> Text

	Table    <- (Blank '{|' {| Attr* |} {| TableRow* |} TableRowSep? TableEnd)
	         -> Table
	TableEnd <- Blank '|}'

	TableRow    <- TableRowSep? {| TableCell+ |}
	            -> TableRow
	TableRowSep <- (Space %nl Blank '|-')+

	TableCell           <- (TableCellSep {| (Attr* Space '|')? |} Blank? {| (TableCellText / NonText)* |})
	                    -> TableCell
	TableCellSep        <- Space (TableCellNewlineSep / TableCellInlineSep)
	TableCellNewlineSep <- %nl Blank ('|' !('-' / '}') / '!')
	TableCellInlineSep  <- '||' / '!!'
	TableCellText       <- {| ({ (!(TableCellSep / TableRowSep / TableEnd / NonText / SpecialChar) .)+ } / SpecialChar)+ |} -> Text

	SpecialChar <- '&' [a-zA-Z0-9]+ -> SpecialChar ';'

	Identifier <- Blank { [a-zA-Z_-]* }

	Comment <- '<!--' (!'-->' .)* '-->'
	Blank   <- (%s / Comment)*
	Space   <- (!%nl %s / Comment)*
]=], {
	Bold = function(content)
		return {
			type = 'Bold',
			content = content
		}
	end,

	Italic = function(content)
		return {
			type = 'Italic',
			content = content
		}
	end,

	Link = function(target, content)
		return {
			type = 'Link',
			target = target,
			content = content or { { type = 'Text', content = target } }
		}
	end,

	Template = function(target, content)
		return {
			type = 'Template',
			target = target,
			content = content
		}
	end,

	Element = function(startTag, content, endTag)
		return {
			type = 'Element',
			startTag = startTag,
			content = content,
			endTag = endTag
		}
	end,

	StartTag = function(name, attrs)
		return {
			name = name,
			attrs = attrs
		}
	end,

	EndTag = function(name)
		return {
			name = name
		}
	end,

	Attr = function(name, value)
		return {
			name = name,
			value = value
		}
	end,

	Table = function(attrs, rows)
		return {
			type = 'Table',
			attrs = attrs,
			rows = rows
		}
	end,

	TableRow = function(cells)
		return {
			type = 'TableRow',
			cells = cells
		}
	end,

	TableCell = function(attrs, content)
		return {
			type = 'TableCell',
			attrs = attrs,
			content = content
		}
	end,

	SpecialChar = function(id)
		return specialChars[id] or ''
	end,

	Text = function(content)
		return {
			type = 'Text',
			content = table.concat(content)
		}
	end,
})

return function(page)
	return lpeg.match(grammar, page)
end
