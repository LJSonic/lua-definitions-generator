local parsePage = require 'parsePage'
local extractFunction = require 'extractFunction'
local util = require 'util'

local doc = {}

local function appendDoc(s)
	table.insert(doc, s)
end

local page = parsePage(util.readFile('wiki/Functions.txt'))

for _, t in ipairs(page) do
	local headers = t.rows[1]
	if headers then
		local cell = headers.cells[1]
		if util.wikiToPlainString(cell.content) == 'Function' then
			for i = 2, #t.rows do
				appendDoc(extractFunction(t.rows[i], nil, 'Functions'))
			end
		end
	end
end

doc = table.concat(doc):gsub('\n*$', '\n')
util.writeFile('defs/Functions.lua', doc)
